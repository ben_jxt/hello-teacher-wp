<?php

define('PAGE_TEACHER_LOGIN', 121);
define('PAGE_TEACHER_REGISTER_SUCCESS', 111);
define('PAGE_LESSON_PLANS', 67);
define('PAGE_ADD_LESSON_PLAN_SUCCESS', 67);
define('PAGE_EDIT_LESSON_PLAN', 75);

function jxt_auth_redirect()
{
    if (is_user_logged_in())
    {
        return;
    }

    $redirect = ( strpos($_SERVER['REQUEST_URI'], '/options.php') && wp_get_referer() ) ? wp_get_referer() : set_url_scheme('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

    $login_url = get_permalink(PAGE_TEACHER_LOGIN);
    $login_url = add_query_arg('redirect_to', urlencode($redirect), $login_url);

    wp_redirect($login_url);

    exit();
}

function generate_captcha($name)
{
    $num_1 = intval(rand(1, 10));
    $num_2 = intval(rand(1, 10));

    if ($num_1 > $num_2)
    {
        $captcha = "{$num_1} - {$num_2}";

        $captcha_ans = $num_1 - $num_2;
    }
    else
    {
        if ($num_1 === $num_2)
        {
            if ($num_2 === 10)
            {
                $num_1--;
            }
            else
            {
                $num_2++;
            }
        }

        $captcha = "{$num_2} - {$num_1}";

        $captcha_ans = $num_2 - $num_1;
    }

    @session_start();
    $_SESSION["captcha_{$name}"] = $captcha_ans;

    return $captcha;
}

function validate_captcha($name, $answer)
{
    @session_start();

    if (empty($_SESSION["captcha_{$name}"]))
    {
        return false;
    }

    if (intval($_SESSION["captcha_{$name}"]) !== $answer)
    {
        return false;
    }

    return true;
}

function jxt_list_categories($args = '')
{
    $defaults = array(
        'show_option_all' => '', 'show_option_none' => __('No categories'),
        'orderby' => 'name', 'order' => 'ASC',
        'style' => 'list',
        'show_count' => 0, 'hide_empty' => 1,
        'use_desc_for_title' => 1, 'child_of' => 0,
        'feed' => '', 'feed_type' => '',
        'feed_image' => '', 'exclude' => '',
        'exclude_tree' => '', 'current_category' => 0,
        'hierarchical' => true, 'title_li' => __('Categories'),
        'echo' => 1, 'depth' => 0,
        'taxonomy' => 'category'
    );

    $r = wp_parse_args($args, $defaults);

    if (!isset($r['pad_counts']) && $r['show_count'] && $r['hierarchical'])
    {
        $r['pad_counts'] = true;
    }

    if (true == $r['hierarchical'])
    {
        $r['exclude_tree'] = $r['exclude'];

        $r['exclude'] = '';
    }

    if (!isset($r['class']))
    {
        $r['class'] = ( 'category' == $r['taxonomy'] ) ? 'categories' : $r['taxonomy'];
    }

    if (!taxonomy_exists($r['taxonomy']))
    {
        return false;
    }

    $show_option_all = $r['show_option_all'];
    $show_option_none = $r['show_option_none'];

    $r['walker'] = new JXT_Walker_Category();

    $categories = get_categories($r);

    $output = '';
    if ($r['title_li'] && 'list' == $r['style'])
    {
        $output = '<li class="' . esc_attr($r['class']) . '">' . $r['title_li'] . '<ul>';
    }

    if (empty($categories))
    {
        if (!empty($show_option_none))
        {
            if ('list' == $r['style'])
            {
                $output .= '<li class="cat-item-none">' . $show_option_none . '</li>';
            }
            else
            {
                $output .= $show_option_none;
            }
        }
    }
    else
    {
        if (!empty($show_option_all))
        {
            $posts_page = ( 'page' == get_option('show_on_front') && get_option('page_for_posts') ) ? get_permalink(get_option('page_for_posts')) : home_url('/');

            $posts_page = esc_url($posts_page);

            if ('list' == $r['style'])
            {
                $output .= "<li class='cat-item-all'><a href='$posts_page'>$show_option_all</a></li>";
            }
            else
            {
                $output .= "<a href='$posts_page'>$show_option_all</a>";
            }
        }

        if (empty($r['current_category']) && ( is_category() || is_tax() || is_tag() ))
        {
            $current_term_object = get_queried_object();

            if ($current_term_object && $r['taxonomy'] === $current_term_object->taxonomy)
            {
                $r['current_category'] = get_queried_object_id();
            }
        }

        if ($r['hierarchical'])
        {
            $depth = $r['depth'];
        }
        else
        {
            $depth = -1; // Flat.
        }

        $output .= walk_category_tree($categories, $depth, $r);
    }

    if ($r['title_li'] && 'list' == $r['style'])
    {
        $output .= '</ul></li>';
    }

    if ($r['echo'])
    {
        echo $output;
    }
    else
    {
        return $output;
    }
}

class JXT_Walker_Category extends Walker_Category
{

    public function start_el(&$output, $category, $depth = 0, $args = array(), $id = 0)
    {
        $checked = isset($args['selected']) && is_array($args['selected']) && in_array($category->term_id, $args['selected']);
        

        $link = '<label';

        if ($args['use_desc_for_title'] && !empty($category->description))
        {
            $link .= 'title="' . esc_attr(strip_tags($category->description)) . '"';
        }

        $link .= '>';
        $link .= '<input type="checkbox" data-name="' . $args['name'] . '" value="' . $category->term_id . '"' . ($checked ? ' checked' : '') . ' />';

        $link .= esc_html($category->name) . '</label>';

        if (!empty($args['show_count']))
        {
            $link .= ' (' . number_format_i18n($category->count) . ')';
        }

        if ('list' == $args['style'])
        {
            $output .= "\t<li";

            $class = 'cat-item cat-item-' . $category->term_id;

            if (!empty($args['current_category']))
            {
                $_current_category = get_term($args['current_category'], $category->taxonomy);

                if ($category->term_id == $args['current_category'])
                {
                    $class .= ' current-cat';
                }
                elseif ($category->term_id == $_current_category->parent)
                {
                    $class .= ' current-cat-parent';
                }
            }

            $output .= ' class="' . $class . '"';

            $output .= ">$link\n";
        }
        else
        {
            $output .= "\t$link<br />\n";
        }
    }

}
