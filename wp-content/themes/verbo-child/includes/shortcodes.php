<?php

require_once 'shortcodes/login/class.php';
require_once 'shortcodes/register-teacher/class.php';
require_once 'shortcodes/add-lesson-plan/class.php';
require_once 'shortcodes/edit-lesson-plan/class.php';
require_once 'shortcodes/lesson-plans/class.php';
require_once 'shortcodes/teacher-profile/class.php';
require_once 'shortcodes/lessons-search/class.php';
