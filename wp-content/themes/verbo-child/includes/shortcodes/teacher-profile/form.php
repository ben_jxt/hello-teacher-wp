<form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="ajax-form">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group first_name">
                <label>First name*</label>
                <input type="text" class="form-control" name="first_name" value="<?php echo esc_attr($user->first_name); ?>" />
            </div>

            <div class="form-group last_name">
                <label>Last name*</label>
                <input type="text" class="form-control" name="last_name" value="<?php echo esc_attr($user->last_name); ?>" />
            </div>

            <div class="form-group email">
                <label>Email address*</label>
                <input type="text" class="form-control" name="email" value="<?php echo esc_attr($user->user_email); ?>" disabled />
            </div>

            <hr />

            <div class="form-group password">
                <label>New password</label>
                <input type="password" class="form-control" name="password" />
                <div class="">Leave blank to not change current password</div>
            </div>

            <div class="form-group confirm_password">
                <label>Confirm new password</label>
                <input type="password" class="form-control" name="confirm_password" />
            </div>
        </div>
    </div>

    <div class="form-group actions">
        <button type="submit" class="btn btn-primary">Submit</button>

        <span class="wait"></span>
    </div>

    <input type="hidden" name="action" value="<?php echo $this->action; ?>" />

    <?php wp_nonce_field($this->action, "_{$this->action}"); ?>

    <div class="messages" style="display: none;">
        <div class="generic-success">Saved successfully.</div>
        <div class="generic-error">An error occurred while processing your request. Please try again.</div>
    </div>
</form>
