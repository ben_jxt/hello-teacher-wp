<form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="ajax-form" data-reset="1"  data-redirect="<?php echo isset($_GET['redirect_to']) ? esc_attr($_GET['redirect_to']) : site_url(); ?>">
    <div class="form-group first_name">
        <label>First Name*</label>
        <input type="text" class="form-control" name="first_name" />
    </div>

    <div class="form-group last_name">
        <label>Last Name*</label>
        <input type="text" class="form-control" name="last_name" />
    </div>

    <div class="form-group email">
        <label>Email Address*</label>
        <input type="text" class="form-control" name="email" />
    </div>

    <div class="form-group password">
        <label>Password*</label>
        <input type="password" class="form-control" name="password" />
    </div>

    <div class="form-group confirm_password">
        <label>Confirm Password*</label>
        <input type="password" class="form-control" name="confirm_password" />
    </div>

    <div class="form-group actions">
        <button type="submit" class="btn btn-primary">Submit</button>

        <span class="wait"></span>
    </div>

    <input type="hidden" name="action" value="<?php echo $this->action; ?>" />

    <?php wp_nonce_field($this->action, "_{$this->action}"); ?>

    <div class="messages" style="display: none;">
        <div class="generic-success">Thank you for registering with us. Please check your email for instruction to activate your account.</div>
        <div class="generic-error">An error occurred while processing your request. Please try again.</div>
    </div>
</form>
