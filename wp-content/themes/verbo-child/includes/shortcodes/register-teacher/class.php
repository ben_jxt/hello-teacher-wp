<?php

class JXT_Shortcode_Register_Teacher
{

    protected $tag = 'jxt_register_teacher';
    protected $action = 'jxt_register_teacher';

    public function __construct()
    {
        add_shortcode($this->tag, array($this, 'content'));

        add_action("wp_ajax_{$this->action}", array($this, 'ajaxAction'));
        add_action("wp_ajax_nopriv_{$this->action}", array($this, 'ajaxAction'));
    }

    public function content($atts, $content = '')
    {
        ob_start();

        include 'form.php';

        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

    public function ajaxAction()
    {
        $response = new stdClass();
        $response->errors = array();
        $response->messages = array();

        $this->process($response);

        echo json_encode($response);

        exit;
    }

    protected function process($response)
    {
        // verify the nonce
        $nonce_name = $this->action;
        $field = "_{$nonce_name}";
        $nonce = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (!wp_verify_nonce($nonce, $nonce_name))
        {
            $response->errors['global'][] = 'Invalid form.';

            return;
        }

        // validattion: first_name
        $field = 'first_name';
        $first_name = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (empty($first_name))
        {
            $response->errors[$field][] = 'This field is required.';
        }

        // validattion: last_name
        $field = 'last_name';
        $last_name = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (empty($last_name))
        {
            $response->errors[$field][] = 'This field is required.';
        }

        // validattion: password
        $field = 'password';
        $password = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (empty($password))
        {
            $response->errors[$field][] = 'This field is required.';
        }
        else
        {
            // validattion: confirm_password
            $field = 'confirm_password';
            $confirm_password = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
            if (empty($confirm_password))
            {
                $response->errors[$field][] = 'Please confirm your password.';
            }
            elseif ($password != $confirm_password)
            {
                $response->errors[$field][] = 'Passwords do not match.';
            }
        }

        // validattion: email
        $field = 'email';
        $email = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (empty($email))
        {
            $response->errors[$field][] = 'This field is required.';
        }
        elseif (!is_email($email))
        {
            $response->errors[$field][] = 'Invalid email address.';
        }
        elseif (username_exists($email) || email_exists($email))
        {
            $response->errors[$field][] = 'An account with this email already exists.';
        }

        // if there are errors do not proceed
        if (!empty($response->errors))
        {
            if (empty($response->errors['global']))
            {
                $response->errors['global'][] = 'There are a few input errors. Please check the highlighted areas below.';
            }

            return;
        }

        $user_id = wp_insert_user(array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'user_email' => $email,
            'user_pass' => $password,
            'user_login' => $email,
            'display_name' => trim($first_name . ' ' . $last_name),
            'nickname' => $first_name,
            'role' => 'teacher'
        ));

        if (is_wp_error($user_id))
        {
            foreach ($user_id->get_error_messages() as $msg)
            {
                $response->errors['global'][] = $msg;
            }

            return;
        }

        $activation_key = md5(wp_generate_password());

        add_user_meta($user_id, 'jxt_activation_key', $activation_key);
		
		$response->redirect = get_permalink(PAGE_TEACHER_REGISTER_SUCCESS);
    }

}

new JXT_Shortcode_Register_Teacher();
