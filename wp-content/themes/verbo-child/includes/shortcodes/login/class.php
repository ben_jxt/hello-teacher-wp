<?php

class JXT_Shortcode_Login
{

    protected $tag = 'jxt_login';
    protected $action = 'jxt_login';

    public function __construct()
    {
        add_shortcode($this->tag, array($this, 'content'));

        add_action("wp_ajax_{$this->action}", array($this, 'ajaxAction'));
        add_action("wp_ajax_nopriv_{$this->action}", array($this, 'ajaxAction'));
    }

    public function content($atts, $content = '')
    {
        ob_start();

        include 'form.php';

        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

    public function ajaxAction()
    {
        $response = new stdClass();
        $response->errors = array();
        $response->messages = array();

        $this->process($response);

        echo json_encode($response);

        exit;
    }

    protected function process($response)
    {
        // verify the nonce
        $nonce_name = $this->action;
        $field = "_{$nonce_name}";
        $nonce = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (!wp_verify_nonce($nonce, $nonce_name))
        {
            $response->errors['global'][] = 'Invalid form.';

            return;
        }

        // validattion: username
        $field = 'username';
        $username = isset($_POST[$field]) ? sanitize_user($_POST[$field]) : null;
        if (empty($username))
        {
            $response->errors[$field][] = 'This field is required.';
        }

        // validattion: password
        $field = 'password';
        $password = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (empty($password))
        {
            $response->errors[$field][] = 'This field is required.';
        }

        // validattion: remember
        $field = 'remember';
        $remember = isset($_POST[$field]) ? intval($_POST[$field]) : 0;

        // if there are errors do not proceed
        if (!empty($response->errors))
        {
            if (empty($response->errors['global']))
            {
                $response->errors['global'][] = 'There are a few input errors. Please check the highlighted areas below.';
            }

            return;
        }

        $user = wp_signon(array(
            'user_login' => $username,
            'user_password' => $password,
            'remember' => $remember ? true : false
            ), false);

        if (is_wp_error($user))
        {
            foreach ($user->get_error_messages() as $msg)
            {
                $response->errors['global'][] = $msg;
            }

            return;
        }
		
		$redirect = empty($_POST['redirect_to']) ? get_permalink(PAGE_LESSON_PLANS) : $_POST['redirect_to'];

        $response->redirect = $redirect;
    }

}

new JXT_Shortcode_Login();
