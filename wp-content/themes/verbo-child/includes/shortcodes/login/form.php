<form id="nk_form_login" action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="ajax-form" data-redirect="<?php echo isset($_GET['redirect_to']) ? esc_attr($_GET['redirect_to']) : site_url(); ?>">
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group username">
                <label>Username/Email Address*</label>
                <input type="text" class="form-control" name="username" />
            </div>

            <div class="form-group password">
                <label>Password*</label>
                <input type="password" class="form-control" name="password" />
            </div>

            <div class="form-group remember">
                <label><input type="checkbox" name="remember" value="1" /> Remember me</label>
            </div>

            <div class="form-group">
                <div>
                    <a href="<?php echo site_url('wp-login.php?action=lostpassword'); ?>">Forgot password?</a>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group actions">
        <div>
            <button type="submit" class="btn btn-primary">Submit</button>

            <span class="wait"></span>
        </div>
    </div>

    <input type="hidden" name="action" value="<?php echo $this->action; ?>" />
    <input type="hidden" name="redirect_to" value="<?php echo empty($_GET['redirect_to']) ? '' : $_GET['redirect_to']; ?>" />

    <?php wp_nonce_field($this->action, "_{$this->action}"); ?>

    <div class="messages" style="display: none;">
        <div class="generic-success">You have logged in successfully.</div>
        <div class="generic-error">An error occurred while processing your request. Please try again.</div>
    </div>
</form>
