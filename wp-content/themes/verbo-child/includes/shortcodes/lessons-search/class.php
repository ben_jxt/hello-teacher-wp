<?php

class JXT_Shortcode_LessonsSearch
{

    protected $tag = 'jxt_lessons_search';

    public function __construct()
    {
        add_shortcode($this->tag, array($this, 'content'));
    }

    public function content($atts, $content = '')
    {
        ob_start();

        $keyword = empty($_GET['keyword']) ? '' : sanitize_text_field($_GET['keyword']);
        $category = empty($_GET['category']) ? '' : intval($_GET['category']);
        $grade = empty($_GET['grade']) ? '' : intval($_GET['grade']);

        include 'form.php';

        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

}

new JXT_Shortcode_LessonsSearch();
