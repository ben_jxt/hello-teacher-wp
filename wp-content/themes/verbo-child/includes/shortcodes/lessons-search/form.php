<div class="container">

    <div class="search-holder">

        <img src="<?php echo get_stylesheet_directory_uri(); ?>/media/img/search-header.png" alt="" class="img-responsive">

<form action="<?php echo site_url('/lesson-results'); ?>" method="get" class="form-inline">
  
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Keyword</label>
    <input type="text" name="keyword" value="<?php echo esc_attr($keyword); ?>" placeholder="Enter Keyword(s)" class="form-control" />
  </div>
        



<div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Category</label>
    <?php
            $args = array(
                'show_option_none' => 'Select a school type',
                'orderby' => 'name',
                'order' => 'ASC',
                'child_of' => 0,
                'echo' => 1,
                'selected' => $category,
                'hierarchical' => true,
                'hide_empty' => false,
                'name' => 'category',
                'class' => 'form-control',
                'depth' => 1,
                'taxonomy' => 'lesson_category'
            );

            wp_dropdown_categories($args);
            ?>
    </div>

    <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Keyword</label>
    <?php
            $args = array(
                'show_option_none' => 'Select level',
                'orderby' => 'name',
                'order' => 'ASC',
                'child_of' => 0,
                'echo' => 1,
                'selected' => $grade,
                'hierarchical' => true,
                'hide_empty' => false,
                'name' => 'grade',
                'class' => 'form-control',
                'depth' => 1,
                'taxonomy' => 'lesson_grade'
            );

            wp_dropdown_categories($args);
            ?>
</div>
            
<button type="submit" class="btn btn-dark search-btn"><i class="fa fa-search"></i> Search</button>

        </form>
</div>
</div>
