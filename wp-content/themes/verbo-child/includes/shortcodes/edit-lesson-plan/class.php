<?php

class JXT_Shortcode_EditReview
{

    protected $tag = 'jxt_edit_lesson_plan';
    protected $action = 'jxt_edit_lesson_plan';

    public function __construct()
    {
        add_shortcode($this->tag, array($this, 'content'));

        add_action('wp_head', array($this, 'enqueueScript'));

        add_action("wp_ajax_{$this->action}", array($this, 'ajaxAction'));
    }

    public function content($atts, $content = '')
    {
        $user = wp_get_current_user();

        $id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        if (!$id)
        {
            return '<p>Lesson plan ID is missing.</p>';
        }

        $post = get_post($id);
        if (!$post || $post->post_type != 'lesson_plan' || (!current_user_can('administrator') && $post->post_author != $user->ID))
        {
            return '<p>Lesson plan does not exists.</p>';
        }

        $meta = get_post_meta($post->ID);

        ob_start();

        include 'form.php';

        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

    public function ajaxAction()
    {
        $response = new stdClass();
        $response->errors = array();
        $response->messages = array();

        $this->process($response);

        echo json_encode($response);

        exit;
    }

    public function enqueueScript()
    {
        $theme_url = get_stylesheet_directory_uri();

        wp_enqueue_script('jquery-ui', $theme_url . '/media/js/jquery-file-upload/js/vendor/jquery.ui.widget.js');
        wp_enqueue_script('iframe-transport', $theme_url . '/media/js/jquery-file-upload/js/jquery.iframe-transport.js');
        wp_enqueue_script('jquery-fileupload', $theme_url . '/media/js/jquery-file-upload/js/jquery.fileupload.js');
    }

    protected function process($response)
    {
        // verify the nonce
        $nonce_name = $this->action;
        $field = "_{$nonce_name}";
        $nonce = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (!wp_verify_nonce($nonce, $nonce_name))
        {
            $response->errors['global'][] = 'Invalid form.';

            return;
        }

        $user = wp_get_current_user();

        $id = isset($_POST['id']) ? intval($_POST['id']) : 0;
        if (!$id)
        {
            $response->errors['global'][] = 'Lesson plan ID is missing.';

            return;
        }

        $post = get_post($id);
        if (!$post || $post->post_type != 'lesson_plan' || (!current_user_can('administrator') && $post->post_author != $user->ID))
        {
            $response->errors['global'][] = 'Lesson plan does not exists.';

            return;
        }

        // validattion: plan_name
        $field = 'plan_name';
        $plan_name = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;
        if (empty($plan_name))
        {
            $response->errors[$field][] = 'This field is required.';
        }

        // validattion: lesson_grade
        $field = 'lesson_grade';
        $lesson_grade = isset($_POST[$field]) ? intval($_POST[$field]) : 0;

        // validattion: lesson_category
        $field = 'lesson_category';
        $lesson_category = isset($_POST[$field]) ? intval($_POST[$field]) : 0;

        // validattion: excerpt
        $field = 'excerpt';
        $excerpt = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;

        // validattion: description
        $field = 'description';
        $description = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;

        // validattion: date
        $field = 'date';
        $date = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;

        // validattion: teacher_user_relationship
        $field = 'teacher_user_relationship';
        $teacher_user_relationship = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;

        // validattion: lesson_thumbnail
        $field = 'lesson_thumbnail';
        $lesson_thumbnail = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;

        // validattion: file1
        $field = 'file1';
        $file1 = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;

        // validattion: file2
        $field = 'file2';
        $file2 = isset($_POST[$field]) ? sanitize_text_field($_POST[$field]) : null;

        // validattion: ratings
        $field = 'ratings';
        $ratings = isset($_POST[$field]) ? intval($_POST[$field]) : 0;

        // if there are errors do not proceed
        if (!empty($response->errors))
        {
            if (empty($response->errors['global']))
            {
                $response->errors['global'][] = 'There are a few input errors. Please check the highlighted areas below.';
            }

            return;
        }

        $data = array(
            'ID' => $post->ID,
            'post_title' => $plan_name,
            'post_content' => $description,
            'post_excerpt' => $excerpt,
        );

        $post_id = wp_update_post($data, true);

        if (is_wp_error($post_id))
        {
            foreach ($post_id->get_error_messages() as $msg)
            {
                $response->errors['global'][] = $msg;
            }

            return;
        }

        // post meta data
        update_post_meta($post_id, 'ratings', $ratings);
        update_post_meta($post_id, 'date', $date);
        update_post_meta($post_id, 'teacher_user_relationship', $teacher_user_relationship);

        // set grade
        if ($lesson_grade)
        {
            wp_set_object_terms($post_id, $lesson_grade, 'lesson_grade');
        }

        // set category
        if ($lesson_category)
        {
            wp_set_object_terms($post_id, $lesson_category, 'lesson_category');
        }

        if ($lesson_thumbnail)
        {
            $thumbnail_id = get_post_thumbnail_id($post->ID);
            if ($thumbnail_id)
            {
                $thumbnail = get_post_meta($thumbnail_id, '_wp_attached_file', true);
                $thumbnail = trim($thumbnail, '/');
            }
            else
            {
                $thumbnail = '';
            }

            if ($thumbnail != trim($lesson_thumbnail, '/'))
            {
                $wp_upload_dir = wp_upload_dir();

                $wp_filetype = wp_check_filetype(basename($lesson_thumbnail), null);

                $attachment = array(
                    'guid' => $wp_upload_dir['baseurl'] . '/' . $lesson_thumbnail,
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename($lesson_thumbnail)),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );

                $filename = $wp_upload_dir['basedir'] . $lesson_thumbnail;

                $attach_id = wp_insert_attachment($attachment, $filename, $post_id);
                $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                wp_update_attachment_metadata($attach_id, $attach_data);
                set_post_thumbnail($post_id, $attach_id);
            }
        }

        // set file 1
        if ($file1)
        {
            $attach_id = intval(get_post_meta($post_id, 'file_1', true));
            if ($attach_id)
            {
                $file = get_post_meta($attach_id, '_wp_attached_file', true);
                $file = trim($file, '/');
            }
            else
            {
                $file = '';
            }

            if ($file != trim($file1, '/'))
            {
                $wp_upload_dir = wp_upload_dir();

                $wp_filetype = wp_check_filetype(basename($file1), null);

                $attachment = array(
                    'guid' => $wp_upload_dir['baseurl'] . '/' . $file1,
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename($file1)),
                    'post_content' => '',
                    'post_parent' => $post_id,
                    'post_status' => 'inherit'
                );

                $filename = $wp_upload_dir['basedir'] . $file1;

                $attach_id = wp_insert_attachment($attachment, $filename, $post_id);
                $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                wp_update_attachment_metadata($attach_id, $attach_data);

                update_post_meta($post_id, 'file_1', $attach_id);
            }
        }

        // set file 2
        if ($file2)
        {
            $attach_id = intval(get_post_meta($post_id, 'file_2', true));
            if ($attach_id)
            {
                $file = get_post_meta($attach_id, '_wp_attached_file', true);
                $file = trim($file, '/');
            }
            else
            {
                $file = '';
            }

            if ($file != trim($file2, '/'))
            {
                $wp_upload_dir = wp_upload_dir();

                $wp_filetype = wp_check_filetype(basename($file2), null);

                $attachment = array(
                    'guid' => $wp_upload_dir['baseurl'] . '/' . $file2,
                    'post_mime_type' => $wp_filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename($file2)),
                    'post_content' => '',
                    'post_parent' => $post_id,
                    'post_status' => 'inherit'
                );

                $filename = $wp_upload_dir['basedir'] . $file2;

                $attach_id = wp_insert_attachment($attachment, $filename, $post_id);
                $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                wp_update_attachment_metadata($attach_id, $attach_data);

                update_post_meta($post_id, 'file_2', $attach_id);
            }
        }
    }

}

new JXT_Shortcode_EditReview();
