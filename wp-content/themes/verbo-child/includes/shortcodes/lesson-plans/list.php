<?php
if(!empty($_GET['success']))
{
	echo '<div class="alert alert-success">You lesson plan was added successfully.</div>';
}
?>
<?php if ($query->have_posts()) : ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Lesson Plan</th>
                <th>Date Posted</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php while ($query->have_posts()) : ?>
                <?php
                $query->the_post();
                $post = $query->post;
                $meta_data = get_post_meta($post->ID);
                ?>
                <tr>
                    <td><?php echo esc_html(get_the_title()); ?></td>

                    <td><?php echo get_the_date(); ?></td>

                   

                    <td>
                        <a href="<?php echo $page_lesson_plans . '?action=delete&id=' . get_the_ID(); ?>" title="Delete" onclick="return jxt.deleteLessonPlan();"><span class="icon-remove">Delete</span></a>
                        <a href="<?php echo $page_edit_lesson_plan . '?id=' . get_the_ID(); ?>" title="Edit"><span class="icon-edit">Edit</span></a>
                    </td>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    <?php //sparkling_paging_nav(); ?>
<?php else: ?>
    <p><?php echo 'You haven’t added any lesson plans yet.'; ?> <a href="<?php echo site_url(); ?>/lesson-plans/add-lesson-plan/">Click here</a> to add one.</p>
<?php endif; ?>
