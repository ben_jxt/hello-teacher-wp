<?php

$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
if (!$id)
{
    echo '<div class="alert alert-danger">Lesson plan ID is missing.</div>';
}
else
{
    $post = get_post($id);

    if ($post->post_type != 'lesson_plan' || (!current_user_can('administrator') && $post->post_author != $user->ID))
    {
        echo '<div class="alert alert-danger">Lesson plan does not exists.</div>';
    }
    else
    {
        $result = wp_delete_post($id, true);

        if ($result === false)
        {
            echo '<div class="alert alert-danger">Unable to delete the lesson plan due to an error. Please try again.</div>';
        }
        else
        {
            echo '<div class="alert alert-success">Lesson plan deleted successfully.</div>';
        }
    }
}

echo '<a href="' . get_post_permalink(PAGE_LESSON_PLANS) . '" class="btn btn-primary">Back to Lesson Plans</a>';
