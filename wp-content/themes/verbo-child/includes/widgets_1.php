<?php

/**
 * Lesosn plan search widget class
 */
class JXT_Widget_Lesson_Plan_Search extends WP_Widget
{

    function __construct()
    {
        $widget_ops = array('classname' => 'jxt_widget_lesson_plan_search', 'description' => __("A search form for lesson plans."));

        parent::__construct('jxt_lesson_plan_search', _x('Lesson Plan Search', 'Lesson plan search widget'), $widget_ops);
    }

    function widget($args, $instance)
    {
        extract($args);

        $title = empty($instance['title']) ? '' : $instance['title'];

        echo $before_widget;

        if ($title)
        {
            echo $before_title . $title . $after_title;
        }

        $keyword = empty($_GET['keyword']) ? '' : $_GET['keyword'];

        $grade = empty($_GET['grade']) ? '' : explode(',', $_GET['grade']);
        if ($grade)
        {
            foreach ($grade as $idx => $value)
            {
                $value = intval($value);

                if ($value > 0)
                {
                    $grade[$idx] = $value;
                }
                else
                {
                    unset($grade[$idx]);
                }
            }
        }

        $category = empty($_GET['category']) ? '' : explode(',', $_GET['category']);
        if ($category)
        {
            foreach ($category as $idx => $value)
            {
                $value = intval($value);

                if ($value > 0)
                {
                    $category[$idx] = $value;
                }
                else
                {
                    unset($category[$idx]);
                }
            }
        }
        ?>

        <form action="<?php echo site_url('/lesson-results'); ?>" method="get" onsubmit="jxt_process_search_form(this);">
           <!--  <div class="form-group">
                <label>Keyword</label>
                <input type="text" name="keyword" class="form-control" value="<?php //echo esc_attr($keyword); ?>" />
            </div> -->

            

            <div class="form-group">
                <label>School type</label>
                <?php
                $args = array(
                    'show_option_none' => ' ',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'child_of' => 0,
                    'echo' => 1,
                    'selected' => $category,
                    'hierarchical' => true,
                    'hide_empty' => true,
                    'name' => 'category',
                    'depth' => 2,
                    'taxonomy' => 'lesson_category',
                    'title_li' => ''
                );

                echo '<ul class="search-category">';
                jxt_list_categories($args);
                echo '</ul>';
                ?>
            </div>

            <div class="form-group">
                <label>Level</label>
                <?php
                $args = array(
                    'show_option_none' => ' ',
                    'orderby' => 'name',
                    'order' => 'ASC',
                    'child_of' => 0,
                    'echo' => 1,
                    'selected' => $grade,
                    'hierarchical' => true,
                    'hide_empty' => true,
                    'name' => 'grade',
                    'depth' => 2,
                    'taxonomy' => 'lesson_grade',
                    'title_li' => ''
                );

                echo '<ul class="search-grade">';
                jxt_list_categories($args);
                echo '</ul>';
                ?>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-default">Filter results</button>
            </div>

            <input type="hidden" name="grade" />
            <input type="hidden" name="category" />
        </form>

        <script type="text/javascript">
            function jxt_process_search_form(form_el)
            {
                form = jQuery(form_el);

                var idx;
                var data;

                data = new Array();
                idx = 0;
                form.find('input[data-name="grade"]:checked').each(function()
                {
                    data[idx++] = jQuery(this).val();
                });
                form_el.grade.value = data.join(',');

                data = new Array();
                idx = 0;
                form.find('input[data-name="category"]:checked').each(function()
                {
                    data[idx++] = jQuery(this).val();
                });
                form_el.category.value = data.join(',');
            }
        </script>

        <?php
        echo $after_widget;
    }

    function form($instance)
    {
        $instance = wp_parse_args((array) $instance, array('title' => ''));

        $title = $instance['title'];
        ?>

        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>

        <?php
    }

    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;

        $new_instance = wp_parse_args((array) $new_instance, array('title' => ''));

        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;
    }

}

/**
 * Register widgets.
 */
function jxt_widgets_init()
{
    register_widget('JXT_Widget_Lesson_Plan_Search');
}

add_action('widgets_init', 'jxt_widgets_init');
