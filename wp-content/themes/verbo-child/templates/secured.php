<?php
jxt_auth_redirect();

/**
 * Template Name: Secured
 */
get_header();
?>

 <div class="mythemes-page-header">

              <div class="container">
                <div class="row">

                  <div class="col-lg-12">
                    <h1><?php the_title(); ?></h1>
                    <nav class="mythemes-nav-inline">
                      <ul class="mythemes-menu">
                        <li><a href="<?php echo home_url(); ?>" title="<?php _e( 'go home' , 'myThemes' ); ?>"><i class="icon-home"></i> <?php _e( 'Home' , 'myThemes' ); ?></a></li>
                        <li><?php the_title(); ?></li>
                      </ul>
                    </nav>
                  </div>

                </div>
              </div>

            </div>

            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
<div id="primary" class="content-area">

    <main id="main" class="site-main" role="main">

        <?php while (have_posts()) : the_post(); ?>

           <?php the_content(); ?>

            <?php
            // If comments are open or we have at least one comment, load up the comment template
            //if (comments_open() || '0' != get_comments_number()) :
            //    comments_template();
            //endif;
            ?>

        <?php endwhile; // end of the loop.  ?>

    </main><!-- #main -->
</div><!-- #primary -->

</div>
<div class="col-sm-3">
  <?php get_sidebar(); ?>
</div>

      </div>
                </div>
            </div>





<?php get_footer(); ?>