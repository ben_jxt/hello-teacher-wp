<?php
/**
 * Template Name: Lesson Results
 */
$searched = false;

$tax_query = array();

$keyword = empty($_GET['keyword']) ? '' : $_GET['keyword'];
if ($keyword)
{
    $searched = true;
}

$grade = empty($_GET['grade']) ? '' : explode(',', $_GET['grade']);
if ($grade)
{
    foreach ($grade as $idx => $value)
    {
        $value = intval($value);

        if ($value > 0)
        {
            $grade[$idx] = $value;
        }
        else
        {
            unset($grade[$idx]);
        }
    }

    if (!empty($grade))
    {
        $searched = true;

        $tax_query[] = array(
            'taxonomy' => 'lesson_grade',
            'field' => 'term_id',
            'terms' => $grade
        );
    }
}

$category = empty($_GET['category']) ? '' : explode(',', $_GET['category']);
if ($category)
{
    foreach ($category as $idx => $value)
    {
        $value = intval($value);

        if ($value > 0)
        {
            $category[$idx] = $value;
        }
        else
        {
            unset($category[$idx]);
        }
    }

    if (!empty($category))
    {
        $searched = true;

        $tax_query[] = array(
            'taxonomy' => 'lesson_category',
            'field' => 'term_id',
            'terms' => $category
        );
    }
}

if (!empty($tax_query))
{
    $tax_query['relation'] = 'AND';
}

$query = new WP_Query(array(
    'post_type' => 'lesson_plan',
    'post_status' => 'publish',
    's' => $keyword,
    'tax_query' => $tax_query
    ));

get_header('lesson-plan');
?>

<?php echo do_shortcode("[jxt_lessons_search]"); ?>

<div class="content">
    <div class="container">
        <div class="row">

            <div class="col-sm-9">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">
                        <h1>Your search returned <?php echo $query->found_posts ?> lesson plans</h1>
                        <?php if ($query->have_posts()) : ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                    <div class="blog-item-wrap">

                                        <div class="post-inner-content">
                                            <div class="thumb-holder1">
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                                    <?php the_post_thumbnail('sparkling-featured', array('class' => 'single-featured thumbnail pull-left lesson-feature-img')); ?>
                                                </a>
                                            </div>
                                            <header class="entry-header page-header">
                                                <h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                                                <div class="entry-meta">
                                                    <?php //sparkling_posted_on();  ?>
                                                    <strong>By:</strong> <?php the_author(); ?>
                                                    &nbsp;&nbsp;<strong>Category:</strong> <?php the_terms($post->id, 'lesson_category'); ?>
                                                    &nbsp;&nbsp;<strong>Grade:</strong> <?php the_terms($post->id, 'lesson_grade'); ?>
                                                </div>
                                            </header>
                                            <div class="entry-summary">
                                                <?php the_excerpt(); ?>
                                                 <?php echo do_shortcode('[shareaholic app="share_buttons" id="11402183"]'); ?>
                                                <p><a class="btn btn-default read-more" href="<?php the_permalink(); ?>"><?php _e('Read More', 'sparkling'); ?></a></p>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </article>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php get_template_part('content', 'none'); ?>
                        <?php endif; ?>
                    </main>
                </div>
            </div>

            <div class="col-sm-3">
                <?php get_sidebar(); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>