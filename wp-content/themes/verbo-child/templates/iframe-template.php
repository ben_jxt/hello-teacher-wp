<?php
/**
 * Template Name: iframe lessons
 */
$searched = false;

$tax_query = array();

$keyword = empty($_GET['keyword']) ? '' : $_GET['keyword'];
if ($keyword)
{
    $searched = true;
}

$grade = empty($_GET['grade']) ? '' : explode(',', $_GET['grade']);
if ($grade)
{
    foreach ($grade as $idx => $value)
    {
        $value = intval($value);

        if ($value > 0)
        {
            $grade[$idx] = $value;
        }
        else
        {
            unset($grade[$idx]);
        }
    }

    if (!empty($grade))
    {
        $searched = true;

        $tax_query[] = array(
            'taxonomy' => 'lesson_grade',
            'field' => 'term_id',
            'terms' => $grade
        );
    }
}

$category = empty($_GET['category']) ? '' : explode(',', $_GET['category']);
if ($category)
{
    foreach ($category as $idx => $value)
    {
        $value = intval($value);

        if ($value > 0)
        {
            $category[$idx] = $value;
        }
        else
        {
            unset($category[$idx]);
        }
    }

    if (!empty($category))
    {
        $searched = true;

        $tax_query[] = array(
            'taxonomy' => 'lesson_category',
            'field' => 'term_id',
            'terms' => $category
        );
    }
}

if (!empty($tax_query))
{
    $tax_query['relation'] = 'AND';
}

$query = new WP_Query(array(
    'post_type' => 'lesson_plan',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    's' => $keyword,
    'tax_query' => $tax_query
    ));

?>

<style>
    .featuredListings a {
text-decoration: none;
font-weight: bold;
color: #C40001;
font-family: Verdana, Geneva, sans-serif;
font-size: 14px;

}

.featuredListings { margin:10px 0 3px; padding:0px;}

</style>

                        <?php if ($query->have_posts()) : ?>
                            <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                    <h1 class="featuredListings"><a href="<?php the_permalink(); ?>" rel="bookmark" target="_blank"><?php echo substr(get_the_title(), 0,25); ?> ...</a></h1>
                                    <?php echo substr(get_the_excerpt(), 0,20); ?> ...
                                            
                                          
                                   
                                </article>
                            <?php endwhile; ?>
                        
                        <?php endif; ?>
         