<?php

include 'UploadHandler.php';

class UploadHandler2 extends UploadHandler
{

    protected function upcount_name_callback($matches)
    {
        $index = isset($matches[1]) ? intval($matches[1]) + 1 : 1;

        $ext = isset($matches[2]) ? $matches[2] : '';

        return '-' . $index . $ext;
    }

    protected function upcount_name($name)
    {
        return preg_replace_callback('/(?:(?:-([\d]+))?(\.[^.]+))?$/', array($this, 'upcount_name_callback'), $name, 1);
    }

}
