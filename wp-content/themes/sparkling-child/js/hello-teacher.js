jQuery(document).ready(function ($) {


/* Add default option to drop-downs */
$( "#job-search .wpjb-field select option:first-child" ).empty();
$( "#job-search .wpjb-field select#type option:first-child" ).append( "Job Type" );
$( "#job-search .wpjb-field select#category option:first-child" ).append( "Job Category" );
$( "#job-search .wpjb-field select#schooltype option:first-child" ).append( "Any School Type" );
$( "#internal-job-search .wpjb-field select#schooltype option:first-child" ).append( "Any School Type" );

/* Add modal popup to sign in and register */
// $("#menu-primary-menu > li:last-child a").removeAttr("href").attr("data-toggle","modal").attr("data-target","#register-in-popup");
// $("#menu-primary-menu > li:nth-last-child(2) a").removeAttr("href").attr("data-toggle","modal").attr("data-target","#sign-in-popup");

/* Add placeholders to job search on homepage */
$( "#job-search .wpjb-element-name-query input" ).attr( "placeholder", "Enter keyword(s)");

/* Add icons to sign/register buttons on header */
$( ".navbar-nav .header-sign-in" ).prepend( "<i class='icon-user'></i>" );
$( ".navbar-nav .header-register" ).prepend( "<i class='icon-pencil'></i>" );

/* Label tooltips for homepage search */
$( "#job-search .wpjb-element-name-location label" ).append ( "<a class='tip' href='#' data-toggle='tooltip' title='' data-original-title='Geography’s not your strong point? See the map below.'><i class='icon-question-sign'></i></a>" );

$('.tip').tooltip();
$('select').customSelect();

// placeholder for older browsers
	if( jQuery.isFunction( 'placeholder' ) ) {
    	$('input, textarea').placeholder();
	}
	
	
        $('.wpjb-expand-map').click( function(event) {
            event.preventDefault();
            $(".wpjb-expanded-map iframe").attr("src", $(this).attr("href"));
            $('.wpjb-expanded-map iframe').slideToggle( 'slow' );
            return false;
        });

        $(".wpjb-ls-type-main").click(function(e) {
            e.preventDefault();
        });

        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            $(".wpjb-ls-type-main").click(function(e) {
                $(".wpjb-sub-filters").toggle();
            });
        }


});

jQuery(document).ready(function() {
    jQuery('#location').attr('placeholder', 'Enter your location');
    jQuery('#schooltype').attr('placeholder', 'Select school type');
    jQuery('#category').attr('placeholder', 'Select job type');
});
