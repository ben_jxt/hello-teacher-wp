var jxt = {};

jxt.scrollTo = function(element, callback, adjust)
{
    adjust = adjust || 50;

    if (typeof (element) !== "object")
    {
        element = jQuery(element);
    }

    if (element.length === 0)
    {
        return;
    }

    if (jQuery(window).scrollTop() > element.offset().top)
    {
        var destination = element.offset().top;

        jQuery("html:not(:animated),body:not(:animated)").animate(
        {
            scrollTop: destination - adjust
        },
        500, callback);
    }
    else
    {
        if (callback)
        {
            callback();
        }
    }
};



jxt.showFieldErrorMessage = function(container, field, message)
{
    var container = jQuery(container);

    if (container.length === 0)
    {
        return;
    }

    var field = container.find('.form-group.' + field);

    if (field.length === 0)
    {
        return;
    }

    field.addClass('has-error');

    var alert_el = field.find('.help-danger');

    if (alert_el.length === 0)
    {
        alert_el = '<div class="help-block help-danger">';

        alert_el += '<div class="message"></div>';

        alert_el += '</div>';

        alert_el = jQuery(alert_el);

        field.append(alert_el);
    }

    alert_el.find('.message').html(message);

    if (container.hasClass('tooltip-messages'))
    {
        var input = field.find('input:first, select:first, textarea:first');

        if (input.length !== 0)
        {
            var input_h = input.height() + parseInt(input.css('padding-top')) + parseInt(input.css('padding-bottom'));
            var alert_h = alert_el.height() + parseInt(alert_el.css('padding-top')) + parseInt(alert_el.css('padding-bottom'));

            alert_el.css('left', input.position().left + input.width() + 45);
            alert_el.css('top', input.position().top + ((input_h / 2) - (alert_h / 2)));
        }
    }
};

jxt.showErrorMessage = function(container, message)
{
    var container = jQuery(container);

    if (container.length === 0)
    {
        return;
    }

    var alert_el = container.find('.alert-danger');

    if (alert_el.length === 0)
    {
        alert_el = '<div class="alert alert-danger alert-dismissable">';

        alert_el += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

        alert_el += '<div class="message"></div>';

        alert_el += '</div>';

        alert_el = jQuery(alert_el);

        container.prepend(alert_el);
    }

    alert_el.find('.message').html(message);
};

jxt.showSuccessMessage = function(container, message)
{
    var container = jQuery(container);

    if (container.length === 0)
    {
        return;
    }

    var alert_el = container.find('.alert-success');

    if (alert_el.length === 0)
    {
        alert_el = '<div class="alert alert-success alert-dismissable">';

        alert_el += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';

        alert_el += '<div class="message"></div>';

        alert_el += '</div>';

        alert_el = jQuery(alert_el);

        container.prepend(alert_el);
    }

    alert_el.find('.message').html(message);
};

jxt.submitJSONForm = function(form_el)
{
    var form = jQuery(form_el);

    var messages = form.find('.messages');

    jQuery('.wait', form).show();

    jQuery('button', form).attr('disabled', 'disabled').addClass('disabled');

    try
    {
        tinyMCE.triggerSave(true, true);
    }
    catch (err)
    {

    }

    jQuery.ajax(
    {
        type: 'POST',
        url: form.attr('action'),
        cache: false,
        dataType: 'json',
        data: form.serialize()
    })
    .done(function(response)
    {
        jQuery(".alert-danger", form).remove();
        jQuery(".alert-success", form).remove();

        jQuery(".form-group .help-danger", form).remove();
        jQuery(".form-group .help-success", form).remove();

        jQuery(".form-group", form).removeClass("has-error");
        jQuery(".form-group", form).removeClass("has-success");

        try
        {
            // set captcha if present
            if (response.captcha)
            {
                form.find('.captcha-ques').html(response.captcha);

                form.find('input[name="captcha"]').val('');
            }

            if (response.errors.length === 0)
            {
                if (form.attr('data-reset') === '1')
                {
                    form_el.reset();
                }

                if (response.redirect)
                {
                    window.location.href = response.redirect;
                }
                else if (typeof form.attr('data-redirect') !== 'undefined' && form.attr('data-redirect') !== '')
                {
                    window.location.href = form.attr('data-redirect');
                }
                else
                {
                    jxt.showSuccessMessage(form, messages.find('.generic-success').html());
                }
            }
            else
            {
                var global = '';

                for (var key in response.errors)
                {
                    if (key === 'global' || key === '')
                    {
                        for (var i in response.errors.global)
                        {
                            global += '<div>' + response.errors.global[i] + '</div>';
                        }
                    }
                    else
                    {
                        var html = '';

                        for (var i in response.errors[key])
                        {
                            html += '<div>' + response.errors[key][i] + '</div>';
                        }

                        jxt.showFieldErrorMessage(form, key, html);
                    }
                }

                jxt.showErrorMessage(form, global || messages.find('.generic-error').html());
            }
        }
        catch (err)
        {
            jxt.showErrorMessage(form, err.message);
        }

        jxt.scrollTo(form.find('.alert:first, .has-error:first'));
    })
    .fail(function(jqXHR, textStatus)
    {
        jQuery(".alert-danger", form).remove();
        jQuery(".alert-success", form).remove();

        jQuery(".form-group .help-danger", form).remove();
        jQuery(".form-group .help-success", form).remove();

        jQuery(".form-group", form).removeClass("has-error");
        jQuery(".form-group", form).removeClass("has-success");

        try
        {
            var html = '';

            try
            {
                var response = jQuery.parseJSON(jqXHR.responseText);

                if (response && response.errors.length !== 0)
                {
                    for (var i in response.errors.global)
                    {
                        html = html + '<div class="text">' + response.errors.global[i] + '</div>';
                    }
                }
            }
            catch (err)
            {

            }

            if (html === '')
            {
                html = 'An error occurred while processing your request';
            }

            jxt.showErrorMessage(form, html);
        }
        catch (err)
        {
            jxt.showErrorMessage(form, err.message);
        }

        jxt.scrollTo(form.find('.alert:first, .has-error:first'));
    })
    .complete(function()
    {
        jQuery('.wait', form).hide();

        jQuery('button', form).removeClass('disabled').removeAttr('disabled');
    });
};

jQuery(document).ready(function()
{
    jQuery('.ajax-form').submit(function(event)
    {
        event.preventDefault();

        jxt.submitJSONForm(this);
    });

    jQuery('#upload-thumbnail-btn').each(function()
    {
        var el = jQuery(this);

        var el_progress = jQuery('#upload-thumbnail-progress');

        el.fileupload(
        {
            url: el.attr('data-url'),
            dataType: 'json',
            formData: {action: 'jxt_upload_thumbnail'},
            done: function(e, data)
            {
                var response = data.result;

                for (var i in response.files)
                {
                    var file = response.files[i];

                    if (file.error)
                    {
                        alert(file.name + ' - ' + file.error);

                        continue;
                    }

                    jQuery('#upload-thumbnail-input').val(file.path);
                    jQuery('#upload-thumbnail-name').html(file.name);

                    break;
                }
            },
            fail: function(e, data)
            {
                alert('File upload failed.');
            },
            always: function(e, data)
            {
                setTimeout(function()
                {
                    el_progress.find('.progress .bar').width(0);

                    el_progress.find('.progress .bar .value').html('');

                    el_progress.hide();
                }, 1000);
            },
            start: function(e)
            {
                el_progress.find('.progress .bar').width(0);

                el_progress.find('.progress .bar .value').html('');

                el_progress.show();
            },
            progressall: function(e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                el_progress.find('.progress .bar').width(progress + '%');

                el_progress.find('.progress .bar .value').html(progress + '%');
            }
        });
    });
    
    jQuery('#upload-file1-btn').each(function()
    {
        var el = jQuery(this);

        var el_progress = jQuery('#upload-file1-progress');

        el.fileupload(
        {
            url: el.attr('data-url'),
            dataType: 'json',
            formData: {action: 'jxt_upload_file1'},
            done: function(e, data)
            {
                var response = data.result;

                for (var i in response.files)
                {
                    var file = response.files[i];

                    if (file.error)
                    {
                        alert(file.name + ' - ' + file.error);

                        continue;
                    }

                    jQuery('#upload-file1-input').val(file.path);
                    jQuery('#upload-file1-name').html(file.name);

                    break;
                }
            },
            fail: function(e, data)
            {
                alert('File upload failed.');
            },
            always: function(e, data)
            {
                setTimeout(function()
                {
                    el_progress.find('.progress .bar').width(0);

                    el_progress.find('.progress .bar .value').html('');

                    el_progress.hide();
                }, 1000);
            },
            start: function(e)
            {
                el_progress.find('.progress .bar').width(0);

                el_progress.find('.progress .bar .value').html('');

                el_progress.show();
            },
            progressall: function(e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                el_progress.find('.progress .bar').width(progress + '%');

                el_progress.find('.progress .bar .value').html(progress + '%');
            }
        });
    });
    
    jQuery('#upload-file2-btn').each(function()
    {
        var el = jQuery(this);

        var el_progress = jQuery('#upload-file2-progress');

        el.fileupload(
        {
            url: el.attr('data-url'),
            dataType: 'json',
            formData: {action: 'jxt_upload_file2'},
            done: function(e, data)
            {
                var response = data.result;

                for (var i in response.files)
                {
                    var file = response.files[i];

                    if (file.error)
                    {
                        alert(file.name + ' - ' + file.error);

                        continue;
                    }

                    jQuery('#upload-file2-input').val(file.path);
                    jQuery('#upload-file2-name').html(file.name);

                    break;
                }
            },
            fail: function(e, data)
            {
                alert('File upload failed.');
            },
            always: function(e, data)
            {
                setTimeout(function()
                {
                    el_progress.find('.progress .bar').width(0);

                    el_progress.find('.progress .bar .value').html('');

                    el_progress.hide();
                }, 1000);
            },
            start: function(e)
            {
                el_progress.find('.progress .bar').width(0);

                el_progress.find('.progress .bar .value').html('');

                el_progress.show();
            },
            progressall: function(e, data)
            {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                el_progress.find('.progress .bar').width(progress + '%');

                el_progress.find('.progress .bar .value').html(progress + '%');
            }
        });
    });
});

jxt.deleteLessonPlan = function()
{
    if (!confirm('Are you sure you want to delete this lesson plann?'))
    {
        return false;
    }
};