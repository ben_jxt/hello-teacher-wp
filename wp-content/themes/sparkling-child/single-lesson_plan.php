<?php
/**
 * The Template for displaying all single posts.
 *
 * @package sparkling
 */
get_header('lesson-plan');
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <h1>Lesson Plan Details</h1>

        <?php while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                

                <div class="post-inner-content">
                    <header class="entry-header page-header">

                        <h1 class="entry-title "><?php the_title(); ?></h1>

                        <div class="entry-meta">


                            <?php sparkling_posted_on(); ?>

			             <?php
                            $categories_list = get_the_term_list($post->ID, 'lesson_category');
                            if ($categories_list) :
                                ?>
                                <span class="cat-links"><i class="fa fa-folder-open-o"></i>
                                    <?php printf(__(' %1$s', 'sparkling'), $categories_list); ?>
                                </span>
                            <?php endif; // End if categories ?>

                            <?php
                            $grade_list = get_the_term_list($post->ID, 'lesson_grade');
                            if ($grade_list) :
                                ?>
                                <span class="cat-links"><i class="fa fa-folder-open-o"></i>
                                    <?php printf(__(' %1$s', 'sparkling'), $grade_list); ?>
                                </span>
                            <?php endif; // End if grades ?>

                            <?php edit_post_link(__('Edit', 'sparkling'), '<i class="fa fa-pencil-square-o"></i><span class="edit-link">', '</span>'); ?>
                        </div><!-- .entry-meta -->
                    </header><!-- .entry-header -->

                    <div class="entry-content">

                        <?php
                            // Must be inside a loop.
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('sparkling-featured', array('class' => 'single-featured thumbnail pull-left lesson-plan-feature-img'));
                            }
                            else {
                                echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) . '/images/email.png" class="thumbnail pull-left lesson-plan-feature-img" />';
                            }
                            ?>
                        <?php the_content(); ?>

                        
                        <?php 
                        if (get_field('file_1') || get_field('file_2')) {
                            echo "<h2>Download Plan Files</h2>";
                        }
                        ?>
                        
                        <?php
                        $file = get_field('file_1');
                        if ($file)
                        {
                            echo '<a href="' . esc_attr($file['url']) . '" class="btn btn-primary"><i class="fa fa-download"></i> ' . esc_html($file['title']) . '</a> ';
                        }
                        
                        $file = get_field('file_2');
                        if ($file)
                        {
                            echo '<a href="' . esc_attr($file['url']) . '" class="btn btn-primary"><i class="fa fa-download"></i> ' . esc_html($file['title']) . '</a> ';
                        }
                        ?>

                        <?php
                        wp_link_pages(array(
                            'before' => '<div class="page-links">' . __('Pages:', 'sparkling'),
                            'after' => '</div>',
                            'link_before' => '<span>',
                            'link_after' => '</span>',
                            'pagelink' => '%',
                            'echo' => 1
                        ));
                        ?>
                    </div><!-- .entry-content -->

                    <footer class="entry-meta">
                        <?php if (has_tag()) : ?>
                            <!-- tags -->
                            <div class="tagcloud">

                                <?php
                                $tags = get_the_tags(get_the_ID());
                                foreach ($tags as $tag)
                                {
                                    echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a> ';
                                }
                                ?>

                            </div>
                            <!-- end tags -->
                        <?php endif; ?>
                    </footer><!-- .entry-meta -->
                </div>

                <div class="secondary-content-box">
                    <!-- author bio -->
                    <div class="author-bio content-box-inner">

                        <!-- avatar -->
                        <div class="avatar">
                            <?php echo get_avatar(get_the_author_meta('ID'), '60'); ?>
                        </div>
                        <!-- end avatar -->

                        <!-- user bio -->
                        <div class="author-bio-content">

                            <h4 class="author-name"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>"><?php echo get_the_author_meta('display_name'); ?></a></h4>
                            <p class="author-description">
                                <?php echo get_the_author_meta('description'); ?>
                            </p>

                        </div>
                        <!-- end author bio -->

                    </div>
                    <!-- end author bio -->
                </div>
            </article><!-- #post-## -->

            <?php
				// If comments are open or we have at least one comment, load up the comment template
            if (comments_open() || '0' != get_comments_number()) :
					comments_template();
				endif;
			?>

			<?php sparkling_post_nav(); ?>

        <?php endwhile; // end of the loop.  ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>