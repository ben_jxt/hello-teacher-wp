<?php
$child_theme_dir = get_stylesheet_directory();
// $jxt_auto_load_path = "$child_theme_dir/wpjobboard/plugin/application/libraries";
// Daq_Loader::registerLibrary('JXT', $jxt_auto_load_path);
// require_once "$jxt_auto_load_path/Module/Admin/SchoolType.php";
// require_once "$jxt_auto_load_path/Module/Admin/Salary.php";
// Add your custom functions here
/**
 * Register widgets areas
 *
 */
function header_right_widgets_init()
{
    register_sidebar(array(
        'name' => 'Header Right',
        'id' => 'header_right',
        'before_widget' => '<div class="right">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'header_right_widgets_init');
function jxt_viewmap_one_widgets_init()
{
    register_sidebar(array(
        'name' => 'JXT - View Map Homepage',
        'id' => 'hompepage_viewmap',
        'before_widget' => '<div class="col-sm-4 col-xs-12">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'jxt_viewmap_one_widgets_init');
function jxt_teach_one_widgets_init()
{
    register_sidebar(array(
        'name' => 'JXT - Teach English Homepage',
        'id' => 'hompepage_teach',
        'before_widget' => '<div class="col-sm-8 col-xs-12">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'jxt_teach_one_widgets_init');
function jxt_homepage_one_widgets_init()
{
    register_sidebar(array(
        'name' => 'JXT - Widget One Homepage',
        'id' => 'hompepage_one',
        'before_widget' => '<div class="col-sm-4"><div class="list-section news-list"><i class="icon-star"></i>',
        'after_widget' => '<a href="/view-jobs" class="btn-list-section">view all jobs</a></div></div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'jxt_homepage_one_widgets_init');
function jxt_homepage_two_widgets_init()
{
    register_sidebar(array(
        'name' => 'JXT - Widget Two Homepage',
        'id' => 'hompepage_two',
        'before_widget' => '<div class="col-sm-4">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'jxt_homepage_two_widgets_init');
function jxt_homepage_three_widgets_init()
{
    register_sidebar(array(
        'name' => 'JXT - Widget Three Homepage',
        'id' => 'hompepage_three',
        'before_widget' => '<div class="col-sm-4"><div class="list-section news-list"><i class="icon-rss"></i>',
        'after_widget' => '<a href="/latest-news" class="btn-list-section">read more</a></div></div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'jxt_homepage_three_widgets_init');
function jxt_homepage_four_widgets_init()
{
    register_sidebar(array(
        'name' => 'JXT - Widget Four Homepage',
        'id' => 'hompepage_four',
        'before_widget' => '<div class="col-lg-8 col-sm-6 col-xs-6">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'jxt_homepage_four_widgets_init');
function jxt_homepage_five_widgets_init()
{
    register_sidebar(array(
        'name' => 'JXT - Widget Five Homepage',
        'id' => 'hompepage_five',
        'before_widget' => '<div class="col-lg-4 col-sm-6 col-xs-6">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'jxt_homepage_five_widgets_init');
function wpjobboard_theme_render_step($num, $mark = "<strong>&raquo; {text}</strong>")
{
    $steps = array(
        array(esc_html(Wpjb_Project::getInstance()->conf("seo_step_1")), __("Create ad", "jobeleon")),
        array(esc_html(Wpjb_Project::getInstance()->conf("seo_step_2")), __("Preview", "jobeleon")),
        array(esc_html(Wpjb_Project::getInstance()->conf("seo_step_3")), __("Done!", "jobeleon"))
    );
    $current = $steps[($num - 1)];
    if (strlen($current[0]) == 0)
    {
        $current = $current[1];
    }
    else
    {
        $current = $current[0];
    }
    $currentStep = wpjb_view("current_step");
    if ($currentStep == $num)
    {
        $title = str_replace("{text}", $current, $mark);
        echo '<li class="wpjb-current-step">';
        echo $num . '. ' . $title;
        echo '</li>';
        Wpjb_Project::getInstance()->title = $current;
    }
    else
    {
        echo '<li ' . ( $num < $currentStep ? 'class="wpjb-begone-step"' : '' ) . '>';
        echo $num . '. ' . $current;
        echo '</li>';
    }
}
function wpjobboard_theme_deregister()
{
    wp_deregister_style('wpjb-css');
}
/**
 * Enqueue scripts and styles
 */
function wpjobboard_theme_scripts()
{
    //if(is_wpjb() && wpjb_is_routed_to("index.index")) wp_dequeue_script( 'wpjb-js' );
    wp_dequeue_style('wpjb-css');
    wp_enqueue_style('wpjobboard_theme-style', get_stylesheet_uri());
    // JXT wp_enqueue_script('wpjobboard_theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true);
    wp_enqueue_script('wpjobboard_theme-skip-link-focus-fix', get_stylesheet_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true);
    if (is_singular() && comments_open() && get_option('thread_comments'))
    {
        wp_enqueue_script('comment-reply');
    }
    if (is_singular() && wp_attachment_is_image())
    {
        wp_enqueue_script('wpjobboard_theme-keyboard-image-navigation', get_stylesheet_directory_uri() . '/js/keyboard-image-navigation.js', array('jquery'), '20120202');
    }
    // add input placeholder for... y'know... stuff...
    wp_enqueue_script('wpjobboard_theme_placeholder', get_stylesheet_directory_uri() . '/js/jquery-placeholder/jquery.placeholder.min.js', array('jquery'), '20130105');
    wp_enqueue_script('wpjobboard_theme_customSelect', get_stylesheet_directory_uri() . '/js/jquery.customSelect.min.js', array('jquery'), '20130905');
    // JXT wp_enqueue_script('wpjobboard_theme_scripts', get_stylesheet_directory_uri() . '/js/wpjobboard_theme_scripts.js', array('jquery'), '20130427');
    if (get_option("wpjobboard_theme_ls") && function_exists("is_wpjb") && (is_wpjb() || is_wpjr()))
    {
        wp_enqueue_script('wpjobboard_theme_live_search', get_stylesheet_directory_uri() . '/js/wpjobboard_theme_live_search.js', array('jquery'));
        wp_localize_script('wpjobboard_theme_live_search', 'WpjbLiveSearchLocale', array(
            "no_jobs_found" => __('No job listings found', 'jobeleon'),
            "no_resumes_found" => __('No resumes found', 'jobeleon'),
            "load_x_more" => __('Load %d more', 'jobeleon')
        ));
    }
}
function wpjb_form_get_schooltypes()
{
    $select = Daq_Db_Query::create();
    $select->from("Wpjb_Model_Tag t");
    $select->where("type = ?", JXT_Model_Tag::TYPE_SCHOOL);
    $select->order("title ASC");
    $list = $select->execute();
    $arr = array();
    foreach ($list as $item)
    {
        $arr[] = array(
            "key" => $item->id,
            "value" => $item->id,
            "description" => $item->title
        );
    }
    return apply_filters("wpjb_form_get_schooltypes", $arr);
}
function wpjb_form_get_salaries()
{
    $select = Daq_Db_Query::create();
    $select->from("Wpjb_Model_Tag t");
    $select->where("type = ?", JXT_Model_Tag::TYPE_SALARY);
    $select->order("title ASC");
    $list = $select->execute();
    $arr = array();
    foreach ($list as $item)
    {
        $arr[] = array(
            "key" => $item->id,
            "value" => $item->id,
            "description" => $item->title
        );
    }
    return apply_filters("wpjb_form_get_salaries", $arr);
}
add_action('wp_enqueue_scripts', 'wpjobboard_theme_scripts', 30);
function wpjobboard_theme_form_init_search($form)
{
    $e = $form->create("school", "select");
    $e->setLabel(__("School Type", "wpjobboard"));
    $e->setEmptyOption(true);
    foreach (JXT_Utility_Registry::getSchoolTypes() as $obj)
    {
        $e->addOption($obj->id, $obj->id, $obj->title);
    }
    if (count(JXT_Utility_Registry::getSchoolTypes()) > 0)
    {
        $form->addElement($e, "search");
    }
    $e = $form->create("salary", "select");
    $e->setLabel(__("Salary", "wpjobboard"));
    $e->setEmptyOption(true);
    foreach (JXT_Utility_Registry::getSalaries() as $obj)
    {
        $e->addOption($obj->id, $obj->id, $obj->title);
    }
    if (count(JXT_Utility_Registry::getSalaries()) > 0)
    {
        $form->addElement($e, "search");
    }
}
add_filter('wpjb_form_init_search', 'wpjobboard_theme_form_init_search');
function JXT_wpja_form_init_job($form)
{
    if ($form->isNew())
    {
        $schools = array();
        $salaries = array();
    }
    else
    {
        $schools = $form->getObject()->getTagIds("school");
        $salaries = $form->getObject()->getTagIds("salary");
    }
    $e = $form->create("school", "select");
    $e->setLabel(__("School Type", "wpjobboard"));
    $e->setValue($schools);
    $e->addOptions(wpjb_form_get_schooltypes());
    $form->addElement($e, "job");
    $form->addTag($e);
    $e = $form->create("salary", "select");
    $e->setLabel(__("Salary", "wpjobboard"));
    $e->setValue($salaries);
    $e->addOptions(wpjb_form_get_salaries());
    $form->addElement($e, "job");
    $form->addTag($e);
}
add_filter('wpja_form_init_job', 'JXT_wpja_form_init_job');
add_filter('wpjb_form_init_job', 'JXT_wpja_form_init_job');
function JXT_wpjb_jobs_query($select)
{
    $school = empty($_GET['school']) ? null : $_GET['school'];
    $salary = empty($_GET['salary']) ? null : $_GET['salary'];
    if (!empty($school))
    {
        if (!is_array($school))
        {
            $school = explode(",", $school);
        }
        $select->join("t1.tagged t2school", "t2school.object='job'");
        $select->where("t2school.tag_id IN(?)", array_map("intval", $school));
    }
    if (!empty($salary))
    {
        if (!is_array($salary))
        {
            $salary = explode(",", $salary);
        }
        $select->join("t1.tagged t2salary", "t2salary.object='job'");
        $select->where("t2salary.tag_id IN(?)", array_map("intval", $salary));
    }
    return $select;
}
add_filter('wpjb_jobs_query', 'JXT_wpjb_jobs_query');
function JXT_wpjb_admin_menu($ini)
{
    $ini['school_types'] = array(
        'parent' => 'settings',
        'page_title' => 'School Types',
        'handle' => '/schoolType',
        'access' => 'administrator'
    );
    $ini['salary'] = array(
        'parent' => 'settings',
        'page_title' => 'Salaries',
        'handle' => '/salary',
        'access' => 'administrator'
    );
    return $ini;
}
add_filter('wpjb_admin_menu', 'JXT_wpjb_admin_menu');
/**
 * Added - Amit Kumar
 */
require_once 'includes/functions.php';
require_once 'includes/shortcodes.php';
require_once 'includes/actions.php';
?>
