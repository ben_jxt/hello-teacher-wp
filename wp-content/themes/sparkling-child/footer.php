<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package sparkling
 */
?>
			</div><!-- close .*-inner (main-content or sidebar, depending if sidebar is used) -->
		</div><!-- close .row -->
	</div><!-- close .container -->
</div><!-- close .main-content -->
	<div id="footer-area">
		<div class="container footer-inner">
			<div class="row">
				<?php get_sidebar( 'footer' ); ?>
			</div>
			<div class="row">
					<div class="col-md-6 copyright">
							© <?php echo date('Y'); ?> Hello Teacher! All rights reserved. <a href="/site-map/" class="tip">Sitemap</a> | Powered by <a target="_blank" href="http://www.jxt.com.au">JXT</a>
					</div>	
			</div>
		</div>
	</div>
</div><!-- #page -->
<?php wp_footer(); ?>
</body>
</html>