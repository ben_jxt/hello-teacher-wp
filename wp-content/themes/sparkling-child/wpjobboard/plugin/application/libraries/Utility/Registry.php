<?php

/**
 * Description of Registry
 *
 * @author Amit Kumar
 */
class JXT_Utility_Registry extends Wpjb_Utility_Registry
{

    public static function getSchoolTypes()
    {
        if (self::has("_school_types"))
        {
            return self::get("_school_types");
        }

        $query = Daq_Db_Query::create();
        $query->from("Wpjb_Model_Tag t");
        $query->where("type = ?", JXT_Model_Tag::TYPE_SCHOOL);
        $query->order("title ASC");
        $result = $query->execute();

        self::set("_school_types", $result);

        return $result;
    }

    public static function getSalaries()
    {
        if (self::has("_salaries"))
        {
            return self::get("_salaries");
        }

        $query = Daq_Db_Query::create();
        $query->from("Wpjb_Model_Tag t");
        $query->where("type = ?", JXT_Model_Tag::TYPE_SALARY);
        $query->order("title ASC");
        $result = $query->execute();

        self::set("_salaries", $result);

        return $result;
    }

}
