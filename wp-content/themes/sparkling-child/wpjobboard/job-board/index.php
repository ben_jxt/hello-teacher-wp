<?php
/**
 * Jobs list
 * 
 * This template file is responsible for displaying list of jobs on job board
 * home page, category page, job types page and search results page.
 * 
 * 
 * @author Greg Winiarski
 * @package Templates
 * @subpackage JobBoard
 * 
 * @var $param array List of job search params
 * @var $search_init array Array of initial search params (used only with live search)
 * @var $pagination bool Show or hide pagination
 */
?>

<?php
$all_null = true;

foreach (array("query", "category", "type") as $p) {
    $ls_default[$p] = "";
    if (!isset($param[$p])) {
        $param[$p] = null;
    } else {
        $all_null = false;
    }
}

if (get_option('permalink_structure')) {
    $spoiler = "?";
} else {
    $spoiler = "&";
}

if ($all_null) {
    $spoiler2 = "";
} else {
    $spoiler2 = $spoiler;
}

$search_url = wpjb_link_to("search");

$current_category = null;
$current_type = null;

if ($param["type"] > 0) {
    $current_type = new Wpjb_Model_Tag($param["type"]);
    if (!$current_type->exists() || $current_type->type != "type") {
        $current_type = null;
    }
}

if ($param["category"] > 0) {
    $current_category = new Wpjb_Model_Tag($param["category"]);
    if (!$current_category->exists() || $current_category->type != "category") {
        $current_category = null;
    }
}
?>

<div id="wpjb-main" class="wpjb-page-index">

        <div class="row">
            <div class="col-sm-8"> <h2>
                <?php _e("You're currently browsing", 'jobeleon') ?>:
                <strong class="wpjb-ls-type-title"><?php echo (!$current_type) ? __('All Jobs', 'jobeleon') : esc_html($current_type->title) ?></strong>
                I 
                <strong class="wpjb-ls-category-title"><?php echo (!$current_category) ? __('All Categories', 'jobeleon') : esc_html($current_category->title) ?></strong>
            </h2> 
        </div>
        <div class="col-sm-4"><a href="#" class="wpjb-subscribe pull-right"><img src="<?php esc_attr_e(get_stylesheet_directory_uri() . '/wpjobboard/images/subscribe-icon.png') ?>" alt="<?php _e("Subscribe", "jobeleon") ?>" /></a>
        </div>
        </div>


    <?php wpjb_flash(); ?>

    <table id="wpjb-job-list" class="wpjb-table">
        <tbody>
            <?php $result = wpjb_find_jobs($param) ?>
            <?php if ($result->count) : foreach ($result->job as $job): ?>
                    <?php /* @var $job Wpjb_Model_Job */ ?>
                    <?php $this->job = $job; ?>
                    <?php $this->render("index-item.php") ?>
                    <?php
                endforeach;
            else :
                ?>
                <tr>
                    <td colspan="3" class="wpjb-table-empty">
                        <?php _e("No job listings found.", "jobeleon"); ?>
                    </td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>

    <?php if($pagination): ?>
    <div id="wpjb-paginate-links">
        <?php wpjb_paginate_links($url, $result->pages, $result->page, $query) ?>
    </div>
    <?php endif; ?>


</div>

<?php if (get_option("wpjobboard_theme_ls")): ?>
    <script type="text/javascript">
        jQuery(function($) {
            WPJB_SEARCH_CRITERIA = <?php echo json_encode($search_init) ?>;
            wpjb_ls_jobs_init();
        });
    </script>
<?php endif; ?>

<!-- Begin: Subscribe to anything -->
<?php Wpjb_Project::getInstance()->setEnv("search_feed_url", $result->url->feed); ?>
<?php Wpjb_Project::getInstance()->setEnv("search_params", $param); ?>
<?php add_action("wp_footer", "wpjb_subscribe") ?>
<!-- End: Subscribe to anything -->
