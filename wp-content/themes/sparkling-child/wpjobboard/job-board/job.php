<?php
/**
 * Job details
 * 
 * This template is responsible for displaying job details on job details page
 * (template single.php) and job preview page (template preview.php)
 * 
 * @author Greg Winiarski
 * @package Templates
 * @subpackage JobBoard
 */
/* @var $job Wpjb_Model_Job */
/* @var $company Wpjb_Model_Employer */
$suffix = 'green'; // color scheme
$color_scheme = get_theme_mod('wpjobboard_theme_color_scheme');
$suffix = !empty($color_scheme) ? $color_scheme : $suffix;
?>

<?php $job = wpjb_view("job") ?>


<div class="post-inner-content">

<div itemscope itemtype="http://schema.org/JobPosting">
<meta itemprop="title" content="<?php esc_attr_e($job->job_title) ?>" />
<meta itemprop="datePosted" content="<?php esc_attr_e($job->job_created_at) ?>" />



<?php if ($job->company_name): ?>
    <div class="row">
        <div class="col-sm-3 col-xs-6">Company</div>

        <div class="col-sm-3 col-xs-6">
            <span itemprop="hiringOrganization" itemscope itemtype="http://schema.org/Organization">
                <span itemprop="name">
                    <?php wpjb_job_company($job) ?>
                </span>
            </span>
            <?php wpjb_job_company_profile($job->getCompany(true)) ?>
        </div>
    </div>

    <hr>
<?php endif; ?>

<?php if ($job->locationToString()): ?>
<div class="row">
    <div class="col-sm-3 col-xs-6">
        <?php _e("Location", "jobeleon") ?>
    </div>
    <div class="col-sm-3 col-xs-6">
        <span itemprop="jobLocation" itemscope itemtype="http://schema.org/Place">
            <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <meta itemprop="addressLocality" content="<?php esc_attr_e($job->job_city) ?>" /> 
                <meta itemprop="addressRegion" content="<?php esc_attr_e($job->job_state) ?>" />
                <meta itemprop="addressCountry" content="<?php $country = Wpjb_List_Country::getByCode($job->job_country); esc_attr_e($country["iso2"]) ?>" />
                <meta itemprop="postalCode" content="<?php esc_attr_e($job->job_zip_code) ?>" />

                <a href="#" class="wpjb-tooltip wpjb-expand-map">
                    <img src="<?php echo get_stylesheet_directory_uri() . '/wpjobboard/images/location-' . $suffix . '.png' ?>" alt="" class="wpjb-inline-img" />
                </a>

            <?php if(wpjb_conf("show_maps") && $job->getGeo()->status==2): ?>
                <a href="<?php esc_attr_e(wpjb_google_map_url($job)) ?>" class="wpjb-expand-map"><?php esc_html_e($job->locationToString()) ?></a>
            <?php else: ?>
            <?php esc_html_e($job->locationToString()) ?>
            <?php endif; ?>

    </span>
</span>
</div>
</div>
<hr>

<?php // if (wpjb_conf("show_maps") && $job->getGeo()->status == 2): ?>
<!-- <div class="row">
    <div class="col-sm-12 wpjb-expanded-map"><iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src=""></iframe></div>
    
</div> -->
<!-- <hr> -->
<?php // endif; ?>
<?php endif; ?>

<div class="row">
    <div class="col-sm-3 col-xs-6"><?php _e("Date Posted", "jobeleon") ?></div>
    <div class="col-sm-3 col-xs-6">
        <img src="<?php echo get_stylesheet_directory_uri() . '/wpjobboard/images/calendar-' . $suffix . '.png' ?>" alt="" class="wpjb-inline-img wpjb-tooltip" />
        <?php echo wpjb_job_created_at(get_option('date_format'), $job) ?>
        </div>
</div>
<hr>

<?php if (!empty($job->getTag()->category)): ?>
    <div class="row">
        <div class="col-sm-3 col-xs-6"><?php _e("Category", "jobeleon") ?></div>
        <div class="col-sm-3 col-xs-6">

            <?php foreach ($job->getTag()->category as $category): ?>
            <a href="<?php esc_attr_e(wpjb_link_to("category", $category)) ?>">
                <span itemprop="occupationalCategory"><?php esc_html_e($category->title) ?></span>
            </a>
            <br/>
        <?php endforeach; ?>
    </div>
</div>
<hr>
<?php endif; ?>

<?php if (!empty($job->getTag()->type)): ?>
<div class="row">
    <div class="col-sm-3 col-xs-6">
        <?php _e("Job Type", "jobeleon") ?>
    </div>
    <div class="col-sm-3 col-xs-6">
        <?php foreach ($job->getTag()->type as $type): ?>
                    <a href="<?php esc_attr_e(wpjb_link_to("type", $type)) ?>">
                        <span itemprop="employmentType"><?php esc_html_e($type->title) ?></span>
                    </a>
                    <br/>
        <?php endforeach; ?>
    </div>
</div>
<hr>
<?php endif; ?>

<?php if (!empty($job->getTag()->salary)): ?>
<div class="row">
    <div class="col-sm-3 col-xs-6">
        <?php _e("Salary", "jobeleon") ?>
    </div>
    <div class="col-sm-3 col-xs-6">
        <?php foreach ($job->getTag()->salary as $salary): ?>
                    <span itemprop="employmentType"><?php esc_html_e($salary->title) ?></span>
                    <br/>
        <?php endforeach; ?>
    </div>
</div>
<hr>
<?php endif; ?>

<?php if (!empty($job->getTag()->school)): ?>
<div class="row">
    <div class="col-sm-3 col-xs-6">
        <?php _e("School Type", "jobeleon") ?>
    </div>
    <div class="col-sm-3 col-xs-6">
        <?php foreach ($job->getTag()->school as $school): ?>
                    <span itemprop="employmentType"><?php esc_html_e($school->title) ?></span>
                    <br/>
        <?php endforeach; ?>
    </div>
</div>
<hr>
<?php endif; ?>

<?php foreach($job->getMeta(array("visibility"=>0, "meta_type"=>3, "empty"=>false, "field_type_exclude"=>"ui-input-textarea")) as $k => $value): ?>
    <div class="row">
        <div class="col-sm-3 col-xs-6">
            <?php esc_html_e($value->conf("title")); ?>
        </div>
        <div class="col-sm-3 col-xs-6">
            <?php if ($value->conf("type") == "ui-input-file"): ?>
                        <?php foreach ($job->file->{$value->name} as $file): ?>
                            <a href="<?php esc_attr_e($file->url) ?>" rel="nofollow"><?php esc_html_e($file->basename) ?></a>
                            <?php echo wpjb_format_bytes($file->size) ?><br/>
                        <?php endforeach ?>
                    <?php else: ?>
                        <?php esc_html_e(join(", ", (array) $value->values())) ?>
            <?php endif; ?>
        </div>
    </div>
 <hr>
<?php endforeach; ?>      
<?php do_action("wpjb_template_job_meta_text", $job) ?>

<div class="row">
     <div class="col-sm-12">
        <h3><?php _e("Description", "jobeleon") ?></h3>

    <div itemprop="description" class="wpjb-job-text">

        <?php if ($job->getLogoUrl()): ?>
            <img src="<?php echo $job->getLogoUrl() ?>" id="wpjb-logo" alt="" class="pull-left thumbnail" />
        <?php endif; ?>

        <?php wpjb_rich_text($job->job_description, $job->meta->job_description_format->value()) ?>

    </div>

    <?php foreach($job->getMeta(array("visibility"=>0, "meta_type"=>3, "empty"=>false, "field_type"=>"ui-input-textarea")) as $k => $value): ?>

        <h3><?php esc_html_e($value->conf("title")); ?></h3>
        <div class="wpjb-job-text">
            <?php wpjb_rich_text($value->value(), $value->conf("textarea_wysiwyg") ? "html" : "text") ?>
        </div>

    <?php endforeach; ?>

    <?php do_action("wpjb_template_job_meta_richtext", $job) ?>
    </div>
</div>

</div>

</div>