<?php
/**
 * Template Name: Subscribe Updates
 */
get_header();
?>

<div id="primary" class="content-area">

    <main id="main" class="site-main" role="main">

<main id="main" class="site-main" role="main">

            
<div class="post-inner-content">
<article class="page type-page status-publish hentry">
    <header class="entry-header page-header">
        <h1 class="entry-title">Subscribe to Newsletter</h1>
    </header><!-- .entry-header -->




    <div class="entry-content">
        <p>Fill in your details below to receive the Hello Teacher! newsletter.</p>
        <?php $widgetNL = new WYSIJA_NL_Widget(true);
echo $widgetNL->widget(array('form' => 2, 'form_type' => 'php')); ?>
    </div><!-- .entry-content -->
    </article><!-- #post-## -->
</div>
                
            
        </main>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>