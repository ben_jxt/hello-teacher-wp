<?php
jxt_auth_redirect();

/**
 * Template Name: Secured
 */
get_header();
?>

<div id="primary" class="content-area">

    <main id="main" class="site-main" role="main">

        <?php while (have_posts()) : the_post(); ?>

            <?php get_template_part('content', 'page'); ?>

            <?php
            // If comments are open or we have at least one comment, load up the comment template
            if (comments_open() || '0' != get_comments_number()) :
                comments_template();
            endif;
            ?>

        <?php endwhile; // end of the loop.  ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>

<!-- Custom sidebars to appear on certain page -->
<?php
if (( is_wpjb() || is_wpjr())) :
    dynamic_sidebar('jobboard_one');
    dynamic_sidebar('jobboard_two');
    dynamic_sidebar('jobboard_featured');
endif;
?>

<?php get_footer(); ?>