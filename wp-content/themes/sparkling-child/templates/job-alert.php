<?php
/**
 * Template Name: Job Alert
 */
get_header();
?>

<div id="primary" class="content-area">

    <main id="main" class="site-main" role="main">

<main id="main" class="site-main" role="main">

            
                

<div class="post-inner-content">
<article class="page type-page status-publish hentry">
    <header class="entry-header page-header">
        <h1 class="entry-title">Sign up for Job Alerts!</h1>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php the_widget( 'Wpjb_Widget_Alerts' ); ?> 
    </div><!-- .entry-content -->
    </article><!-- #post-## -->
</div>
                
            
        </main>

    </main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>