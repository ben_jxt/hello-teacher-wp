<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package sparkling
 */
?><!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<!-- favicon -->
<?php if ( of_get_option( 'custom_favicon' ) ) { ?>
<link rel="icon" href="<?php echo of_get_option( 'custom_favicon' ); ?>" />
<?php } ?>
<!--[if IE]><?php if ( of_get_option( 'custom_favicon' ) ) { ?><link rel="shortcut icon" href="<?php echo of_get_option( 'custom_favicon' ); ?>" /><?php } ?><![endif]-->
<?php wp_head(); ?>
<link rel='stylesheet' href='<?php echo get_stylesheet_directory_uri(); ?>/css/jobboard.css' type='text/css' media='all' />
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/hello-teacher.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>
</head>
<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
				<div class="row">
					<div class="site-navigation-inner col-sm-12">
		        <div class="navbar-header">
		            <button type="button" class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		                <span class="sr-only">Toggle navigation</span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		                <span class="icon-bar"></span>
		            </button>
				<?php if( get_header_image() != '' ) : ?>
					<div id="logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"  height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php bloginfo( 'name' ); ?>"/></a>
					</div><!-- end of #logo -->
					
					<div id="header-right">
						<img alt="Hello Teacher" src="<?php echo get_stylesheet_directory_uri(); ?>/images/header-banner.png" class="pull-right img-responsive">
		      		</div>
				<?php endif; // header image was removed ?>
				<?php if( !get_header_image() ) : ?>
					<div id="logo">
						<span class="site-name"><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
					</div><!-- end of #logo -->
				<?php endif; // header image was removed (again) ?>
		        </div>
					<?php sparkling_header_menu(); ?>
					
					<!-- Modal popup -->
					
						<div class="modal fade" id="register-in-popup">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						        <h3 class="modal-title">Register</h4>
						      </div>
						      <div class="modal-body">
						      	<p>Please select user type</p>
						      	<div class="pop-btns-wrap">
						      	<a href="<?php echo site_url(); ?>/resumes/register/" class="btn btn-primary reg-pop-btns">Candidate</a>
						      	<a href="<?php echo site_url(); ?>/jobs/employer/register/" class="btn btn-primary reg-pop-btns">Employer</a>
						        <a href="<?php echo site_url(); ?>/lesson-plans/teacher-registration/" class="btn btn-primary reg-pop-btns">Teacher</a>
        							  </div>
        							  
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      </div>
						    </div><!-- /.modal-content -->
						  </div><!-- /.modal-dialog -->
						</div><!-- /.modal -->

						<div class="modal fade" id="sign-in-popup">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						        <h3 class="modal-title">Sign In</h4>
						      </div>
						      <div class="modal-body">
						      	<p>Please select user type</p>
						      	<div class="pop-btns-wrap">
						      		<a href="<?php echo site_url(); ?>/resumes/login/" class="btn btn-primary login-pop-btns">Candidate</a>
						      		<a href="<?php echo site_url(); ?>/jobs/employer/login/" class="btn btn-primary login-pop-btns">Employer</a>
						      		        						<a href="<?php echo site_url(); ?>/teacher-login/" class="btn btn-primary login-pop-btns">Teacher</a>
						      	</div>
        							  
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      </div>
						    </div><!-- /.modal-content -->
						  </div><!-- /.modal-dialog -->
						</div><!-- /.modal -->
					
					</div>
		    </div>
		  </div>
		</nav><!-- .site-navigation -->
	
		
	</header><!-- #masthead -->
	<div id="content" class="site-content">
			<div class="top-section">
				<?php sparkling_featured_slider(); ?>
				<?php sparkling_call_for_action(); ?>
			</div>
		<div class="container main-content-area">
			<div class="row">
				<?php if(is_front_page() ) { ?>
					<div id="content" class="home-page-container main-content-inner col-sm-12 <?php echo of_get_option( 'site_layout' ); ?>">
				<?php } else { ?>
					<div id="content" class="main-content-inner col-sm-12 col-md-8 <?php echo of_get_option( 'site_layout' ); ?>">
				<?php } ?>
			