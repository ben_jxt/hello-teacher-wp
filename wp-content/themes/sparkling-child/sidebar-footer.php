<?php
/**
 * The Sidebar widget area for footer.
 *
 * @package sparkling
 */
?>

<?php
// If footer sidebars do not have widget let's bail.

if (!is_active_sidebar('footer-widget-1') && !is_active_sidebar('footer-widget-2') && !is_active_sidebar('footer-widget-3'))
	return;
// If we made it this far we must have widgets.
	?>

	<div class="footer-widget-area">
		<?php if ( is_active_sidebar( 'footer-widget-1' ) ) : ?>
		<div class="col-sm-8">
		<div class="col-sm-7 footer-widget" role="complementary">
			<h3 class="widgettitle">Quick Links</h3>
			<?php dynamic_sidebar('footer-widget-1'); ?>
		</div><!-- .widget-area .first -->
		<?php endif; ?>

		<?php if ( is_active_sidebar( 'footer-widget-2' ) ) : ?>
		<div class="col-sm-5 footer-widget" role="complementary">
			<?php dynamic_sidebar('footer-widget-2'); ?>
			<div id="text-9" class="widget widget_text">
				<h3 class="widgettitle">Our Partners</h3>
					<div class="textwidget"><a href="http://www.eagleeyes.com.au/" target="_blank">
						<img alt="Eagle Eyes" src="<?php echo get_stylesheet_directory_uri(); ?>/images/partner-EagleEyes.png" width="201" height="46"></a>
					</div>
			</div>
		</div><!-- .widget-area .second -->
		</div><!-- col-sm-8 -->
		<?php endif; ?>

		<?php if ( is_active_sidebar( 'footer-widget-3' ) ) : ?>
		<div class="col-sm-4 footer-widget" role="complementary">
			<?php dynamic_sidebar('footer-widget-3'); ?>
</div><!-- .widget-area .third -->
<?php endif; ?>
</div>