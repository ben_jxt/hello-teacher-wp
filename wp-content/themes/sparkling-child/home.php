<?php
/**
 * Template Name: Homepage
 */
$keyword = empty($_GET['keyword']) ? '' : sanitize_text_field($_GET['keyword']);
$grade = empty($_GET['grade']) ? 0 : intval($_GET['grade']);
$category = empty($_GET['category']) ? 0 : intval($_GET['category']);
$tax_query = array();
if ($grade > 0)
{
    $tax_query[] = array(
        'taxonomy' => 'lesson_grade',
        'field' => 'term_id',
        'terms' => $grade
    );
}
if ($category > 0)
{
    $tax_query[] = array(
        'taxonomy' => 'lesson_category',
        'field' => 'term_id',
        'terms' => $category
    );
}
if (!empty($tax_query))
{
    $tax_query['relation'] = 'AND';
}
$query = new WP_Query(array(
    'post_type' => 'lesson_plan',
    'post_status' => 'publish',
    's' => $keyword,
    'tax_query' => $tax_query
    ));
get_header('lesson-plan');
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <h1>Your search returned <?php echo $query->found_posts ?> lesson plans</h1>
        <?php if ($query->have_posts()) : ?>
            <?php while ($query->have_posts()) : $query->the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="blog-item-wrap">
                        
                        <div class="post-inner-content">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail('sparkling-featured', array('class' => 'single-featured thumbnail pull-left lesson-feature-img')); ?>
                        </a>
                            <header class="entry-header page-header">
                                <h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                                <div class="entry-meta">
                                    <?php sparkling_posted_on(); ?>
                                    <strong>By:</strong> <?php the_author(); ?>
                                    &nbsp;&nbsp;<strong>Category:</strong> <?php the_terms($post->id, 'lesson_category'); ?>
                                    &nbsp;&nbsp;<strong>Grade:</strong> <?php the_terms($post->id, 'lesson_grade'); ?>
                                </div>
                            </header>
                            <div class="entry-summary">
                                <?php the_excerpt(); ?>
                                <p><a class="btn btn-default read-more" href="<?php the_permalink(); ?>"><?php _e('Read More', 'sparkling'); ?></a></p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </article>
            <?php endwhile; ?>
            <?php sparkling_paging_nav(); ?>
        <?php else : ?>
            <?php get_template_part('content', 'none'); ?>
        <?php endif; ?>
    </main>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>