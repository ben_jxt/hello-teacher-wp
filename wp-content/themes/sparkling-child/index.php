<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package sparkling
 */
get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div id="features" class="row">
				<?php dynamic_sidebar( 'hompepage_teach' ); ?>	
				<?php dynamic_sidebar( 'hompepage_viewmap' ); ?>
			</div>
			<div class="row">
				<?php dynamic_sidebar( 'hompepage_one' ); ?>
<div class="col-sm-4">
	<div class="textwidget">
		<div class="list-section lesson-plans">
		<h2><i class="icon-file"></i> <span>Free Lesson Plans</span></h2>
		<a href="<?php echo site_url(); ?>/" class="btn-list-section" target="_blank">access all plans</a>
				
				<?php
		      $query = new WP_Query(array(
		          'post_type' => 'lesson_plan',
		          'post_status' => 'publish',
		          'posts_per_page' => '2'
		          ));
     
     	if ($query->have_posts())
     	{
     		echo '<ul>';
            while ($query->have_posts())
        	{
        		$query->the_post();
        		echo '<li><a href="' . get_permalink($post->ID) . '">' . get_the_title($post->ID) . '</a>';
        		echo '<p>' . substr(get_the_excerpt(), 0,60) . '</p></li>';
        	}
        	echo '</ul>';
        }	
		?>
	
</div>
</div>
</div>
			
				<?php dynamic_sidebar( 'hompepage_three' ); ?>
			</div>
			<div class="row">
				<?php dynamic_sidebar( 'hompepage_four' ); ?>
				<?php dynamic_sidebar( 'hompepage_five' ); ?>				
			</div>
				
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>
<div class="modal fade" id="map-china" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:980px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title" id="myModalLabel">Map of China</h4>
      </div>
      <div class="modal-body" style="padding:0;">
		  <iframe id="Gmap" src="http://www.makeaclickablemap.com/map.php?49bc0b3664841982336755d2498d1acdfb044a66" frameborder="0" scrolling="no" height="720" width="960"></iframe>
		  </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>