<?php

define('PAGE_TEACHER_LOGIN', 121);
define('PAGE_TEACHER_REGISTER_SUCCESS', 111);
define('PAGE_LESSON_PLANS', 67);
define('PAGE_ADD_LESSON_PLAN_SUCCESS', 67);
define('PAGE_EDIT_LESSON_PLAN', 75);

function jxt_auth_redirect()
{
    if (is_user_logged_in())
    {
        return;
    }

    $redirect = ( strpos($_SERVER['REQUEST_URI'], '/options.php') && wp_get_referer() ) ? wp_get_referer() : set_url_scheme('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

    $login_url = get_permalink(PAGE_TEACHER_LOGIN);
    $login_url = add_query_arg('redirect_to', urlencode($redirect), $login_url);

    wp_redirect($login_url);

    exit();
}

function generate_captcha($name)
{
    $num_1 = intval(rand(1, 10));
    $num_2 = intval(rand(1, 10));

    if ($num_1 > $num_2)
    {
        $captcha = "{$num_1} - {$num_2}";

        $captcha_ans = $num_1 - $num_2;
    }
    else
    {
        if ($num_1 === $num_2)
        {
            if ($num_2 === 10)
            {
                $num_1--;
            }
            else
            {
                $num_2++;
            }
        }

        $captcha = "{$num_2} - {$num_1}";

        $captcha_ans = $num_2 - $num_1;
    }

    @session_start();
    $_SESSION["captcha_{$name}"] = $captcha_ans;

    return $captcha;
}

function validate_captcha($name, $answer)
{
    @session_start();

    if (empty($_SESSION["captcha_{$name}"]))
    {
        return false;
    }

    if (intval($_SESSION["captcha_{$name}"]) !== $answer)
    {
        return false;
    }

    return true;
}
