<form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="ajax-form">
    <div class="form-group plan_name">
        <label>Lesson Plan Name*</label>
        <input type="text" class="form-control" name="plan_name" value="<?php echo esc_attr($post->post_title); ?>" />
    </div>

    <div class="form-group lesson_grade">
        <label>Grade</label>
        <?php
        $grade = 0;
        $terms = wp_get_object_terms($post->ID, 'lesson_grade');
        foreach ($terms as $term)
        {
            if ($term->parent === 0)
            {
                $grade = $term->term_id;

                break;
            }
        }

        $args = array(
            'show_option_none' => ' ',
            'orderby' => 'name',
            'order' => 'ASC',
            'child_of' => 0,
            'echo' => 1,
            'selected' => $grade,
            'hierarchical' => true,
            'hide_empty' => false,
            'name' => 'lesson_grade',
            'class' => 'form-control',
            'depth' => 1,
            'taxonomy' => 'lesson_grade'
        );

        wp_dropdown_categories($args);
        ?>
    </div>

    <div class="form-group lesson_category">
        <label>Category</label>
        <?php
        $category = 0;
        $terms = wp_get_object_terms($post->ID, 'lesson_category');
        foreach ($terms as $term)
        {
            if ($term->parent === 0)
            {
                $category = $term->term_id;

                break;
            }
        }

        $args = array(
            'show_option_none' => ' ',
            'orderby' => 'name',
            'order' => 'ASC',
            'child_of' => 0,
            'echo' => 1,
            'selected' => $category,
            'hierarchical' => true,
            'hide_empty' => false,
            'name' => 'lesson_category',
            'class' => 'form-control',
            'depth' => 1,
            'taxonomy' => 'lesson_category'
        );

        wp_dropdown_categories($args);
        ?>
    </div>

    <div class="form-group excerpt">
        <label>Excerpt</label>
        <textarea class="form-control" name="excerpt"><?php echo esc_html($post->post_excerpt); ?></textarea>
    </div>

    <div class="form-group description">
        <label>Full description</label>
        <?php
        wp_editor($post->post_content, 'description', $settings = array(
            'textarea_name' => 'description',
            'textarea_rows' => 10
        ));
        ?>
    </div>

    <div class="form-group date">
        <label>Date</label>
        <input type="text" class="form-control" name="date" value="<?php echo empty($meta['date'][0]) ? '' : esc_attr($meta['date'][0]); ?>" />
    </div>

    <div class="form-group teacher_user_relationship">
        <label>Teacher User Relationship</label>
        <textarea class="form-control" name="teacher_user_relationship"><?php echo empty($meta['teacher_user_relationship'][0]) ? '' : esc_html($meta['teacher_user_relationship'][0]); ?></textarea>
    </div>

    <div class="form-group lesson_thumbnail">
        <?php
        $thumbnail_id = get_post_thumbnail_id($post->ID);
        if ($thumbnail_id)
        {
            $thumbnail = get_post_meta($thumbnail_id, '_wp_attached_file', true);
        }
        else
        {
            $thumbnail = '';
        }
        ?>

        <label>Thumbnail</label>
        <input id="upload-thumbnail-input" type="hidden" class="form-control" name="lesson_thumbnail" value="" />

        <div class="clearfix">
            <div class="input-group">                                            
                <span id="upload-thumbnail-name" class="form-control"><?php echo esc_html(basename($thumbnail)); ?></span>
                <span class="input-group-btn">
                    <a id="upload-thumbnail-btn" href="javascript:;" type="button" class="btn btn-primary fileinput-button" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        Upload
                        <input type="file" name="files" />
                    </a>
                </span>
            </div>
        </div>

        <div id="upload-thumbnail-progress" class="upload-progress" style="display: none;">
            <div class="progress">
                <div class="progress-bar bar">
                    <div class="value"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group ratings">
        <?php
        $ratings = empty($meta['ratings'][0]) ? '' : intval($meta['ratings'][0]);
        ?>
        <label>Ratings</label>
        <select class="form-control" name="ratings">
            <option value=""></option>
            <option value="1" <?php echo $ratings == 1 ? 'selected' : ''; ?>>1 star</option>
            <option value="2" <?php echo $ratings == 2 ? 'selected' : ''; ?>>2 stars</option>
            <option value="3" <?php echo $ratings == 3 ? 'selected' : ''; ?>>3 stars</option>
            <option value="4" <?php echo $ratings == 4 ? 'selected' : ''; ?>>4 stars</option>
            <option value="5" <?php echo $ratings == 5 ? 'selected' : ''; ?>>5 stars</option>
        </select>
    </div>

    <div class="form-group file1">
        <?php
        $file = empty($meta['file_1'][0]) ? '' : intval($meta['file_1'][0]);
        if ($file)
        {
            $file = get_post_meta($file, '_wp_attached_file', true);
        }
        else
        {
            $file = '';
        }
        ?>
        
        <label>File 1</label>
        <input id="upload-file1-input" type="hidden" class="form-control" name="file1" value="" />

        <div class="clearfix">
            <div class="input-group">                                            
                <span id="upload-file1-name" class="form-control"><?php echo esc_html(basename($file)); ?></span>
                <span class="input-group-btn">
                    <a id="upload-file1-btn" href="javascript:;" type="button" class="btn btn-primary fileinput-button" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        Upload
                        <input type="file" name="files" />
                    </a>
                </span>
            </div>
        </div>

        <div id="upload-file1-progress" class="upload-progress" style="display: none;">
            <div class="progress">
                <div class="progress-bar bar">
                    <div class="value"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group file2">
        <?php
        $file = empty($meta['file_2'][0]) ? '' : intval($meta['file_2'][0]);
        if ($file)
        {
            $file = get_post_meta($file, '_wp_attached_file', true);
        }
        else
        {
            $file = '';
        }
        ?>
        
        <label>File 2</label>
        <input id="upload-file2-input" type="hidden" class="form-control" name="file2" value="" />

        <div class="clearfix">
            <div class="input-group">                                            
                <span id="upload-file2-name" class="form-control"><?php echo esc_html(basename($file)); ?></span>
                <span class="input-group-btn">
                    <a id="upload-file2-btn" href="javascript:;" type="button" class="btn btn-primary fileinput-button" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        Upload
                        <input type="file" name="files" />
                    </a>
                </span>
            </div>
        </div>

        <div id="upload-file2-progress" class="upload-progress" style="display: none;">
            <div class="progress">
                <div class="progress-bar bar">
                    <div class="value"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group actions">
        <button type="submit" class="btn btn-primary">Submit</button>

        <span class="wait"></span>
    </div>

    <input type="hidden" name="id" value="<?php echo $post->ID; ?>" />
    <input type="hidden" name="action" value="<?php echo $this->action; ?>" />

    <?php wp_nonce_field($this->action, "_{$this->action}"); ?>

    <div class="messages" style="display: none;">
        <div class="generic-success">Submitted successfully.</div>
        <div class="generic-error">An error occurred while processing your request. Please try again.</div>
    </div>
</form>
