<div id="wpjb-main" class="wpjb-page-search">
    <form action="<?php echo site_url('/lesson-results'); ?>" method="get" class="wpjb-form">
        <fieldset class="wpjb-fieldset-search" id="lesson-search-widget">
        

        <div class="wpjb-element-input-select">
        
        <label class="wpjb-label">Keyword</label>
        <div class="wpjb-field">
            <input type="text" name="keyword" value="<?php echo esc_attr($keyword); ?>" placeholder="Enter Keyword(s)" class="wpjb-field" />
        </div>
    </div>
            
<div class="wpjb-element-input-select">
            <label class="wpjb-label">Grade</label>
        <div class="wpjb-field">
            
            <?php
            $args = array(
                'show_option_none' => 'Select a grade',
                'orderby' => 'name',
                'order' => 'ASC',
                'child_of' => 0,
                'echo' => 1,
                'selected' => $grade,
                'hierarchical' => true,
                'hide_empty' => false,
                'name' => 'grade',
                'class' => 'hasCustomSelect',
                'depth' => 1,
                'taxonomy' => 'lesson_grade'
            );

            wp_dropdown_categories($args);
            ?>
        </div>

</div>
            
<div class="wpjb-element-input-select">
            <label class="wpjb-label">Category</label>
        <div class="wpjb-field">
            
            <?php
            $args = array(
                'show_option_none' => 'Select a category',
                'orderby' => 'name',
                'order' => 'ASC',
                'child_of' => 0,
                'echo' => 1,
                'selected' => $category,
                'hierarchical' => true,
                'hide_empty' => false,
                'name' => 'category',
                'class' => 'hasCustomSelect',
                'depth' => 1,
                'taxonomy' => 'lesson_category'
            );

            wp_dropdown_categories($args);
            ?>
        </div>
</div>

        </fieldset>

<fieldset>
            <button class="wpjb-submit btn btn-default search-btn"><i class="fa fa-search"></i> Search</button>
        </fieldset>

        </form>

</div>
