<form action="<?php echo admin_url('admin-ajax.php'); ?>" method="post" class="ajax-form" data-ajax-url="<?php echo admin_url('admin-ajax.php'); ?>">
    <div class="form-group plan_name">
        <label>Lesson Plan Name*</label>
        <input type="text" class="form-control" name="plan_name" value="" />
    </div>

    <div class="form-group lesson_grade">
        <label>Grade</label>
        <?php
        $args = array(
            'show_option_none' => ' ',
            'orderby' => 'name',
            'order' => 'ASC',
            'child_of' => 0,
            'echo' => 1,
            'selected' => 0,
            'hierarchical' => true,
            'hide_empty' => false,
            'name' => 'lesson_grade',
            'class' => 'form-control',
            'depth' => 1,
            'taxonomy' => 'lesson_grade'
        );

        wp_dropdown_categories($args);
        ?>
    </div>

    <div class="form-group lesson_category">
        <label>Category</label>
        <?php
        $args = array(
            'show_option_none' => ' ',
            'orderby' => 'name',
            'order' => 'ASC',
            'child_of' => 0,
            'echo' => 1,
            'selected' => 0,
            'hierarchical' => true,
            'hide_empty' => false,
            'name' => 'lesson_category',
            'class' => 'form-control',
            'depth' => 1,
            'taxonomy' => 'lesson_category'
        );

        wp_dropdown_categories($args);
        ?>
    </div>

    <div class="form-group excerpt">
        <label>Excerpt</label>
        <textarea class="form-control" name="excerpt"></textarea>
    </div>

    <div class="form-group description">
        <label>Full description</label>
        <?php
        wp_editor('', 'description', $settings = array(
            'textarea_name' => 'description',
            'textarea_rows' => 10
        ));
        ?>
    </div>

    <div class="form-group date">
        <label>Date</label>
        <input type="text" class="form-control" name="date" value="" />
    </div>

    <div class="form-group teacher_user_relationship">
        <label>Teacher User Relationship</label>
        <textarea class="form-control" name="teacher_user_relationship"></textarea>
    </div>

    <div class="form-group lesson_thumbnail">
        <label>Thumbnail</label>
        <input id="upload-thumbnail-input" type="hidden" class="form-control" name="lesson_thumbnail" value="" />

        <div class="clearfix">
            <div class="input-group">                                            
                <span id="upload-thumbnail-name" class="form-control"></span>
                <span class="input-group-btn">
                    <a id="upload-thumbnail-btn" href="javascript:;" type="button" class="btn btn-primary fileinput-button" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        Upload
                        <input type="file" name="files" />
                    </a>
                </span>
            </div>
        </div>

        <div id="upload-thumbnail-progress" class="upload-progress" style="display: none;">
            <div class="progress">
                <div class="progress-bar bar">
                    <div class="value"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group ratings">
        <label>Ratings</label>
        <select class="form-control" name="ratings">
            <option value=""></option>
            <option value="1">1 star</option>
            <option value="2">2 stars</option>
            <option value="3">3 stars</option>
            <option value="4">4 stars</option>
            <option value="5">5 stars</option>
        </select>
    </div>

    <div class="form-group file1">
        <label>File 1</label>
        <input id="upload-file1-input" type="hidden" class="form-control" name="file1" value="" />

        <div class="clearfix">
            <div class="input-group">                                            
                <span id="upload-file1-name" class="form-control"></span>
                <span class="input-group-btn">
                    <a id="upload-file1-btn" href="javascript:;" type="button" class="btn btn-primary fileinput-button" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        Upload
                        <input type="file" name="files" />
                    </a>
                </span>
            </div>
        </div>

        <div id="upload-file1-progress" class="upload-progress" style="display: none;">
            <div class="progress">
                <div class="progress-bar bar">
                    <div class="value"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group file2">
        <label>File 2</label>
        <input id="upload-file2-input" type="hidden" class="form-control" name="file2" value="" />

        <div class="clearfix">
            <div class="input-group">                                            
                <span id="upload-file2-name" class="form-control"></span>
                <span class="input-group-btn">
                    <a id="upload-file2-btn" href="javascript:;" type="button" class="btn btn-primary fileinput-button" data-url="<?php echo admin_url('admin-ajax.php'); ?>">
                        Upload
                        <input type="file" name="files" />
                    </a>
                </span>
            </div>
        </div>

        <div id="upload-file2-progress" class="upload-progress" style="display: none;">
            <div class="progress">
                <div class="progress-bar bar">
                    <div class="value"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group actions">
        <button type="submit" class="btn btn-primary">Submit</button>

        <span class="wait"></span>
    </div>

    <input type="hidden" name="action" value="<?php echo $this->action; ?>" />

    <?php wp_nonce_field($this->action, "_{$this->action}"); ?>

    <div class="messages" style="display: none;">
        <div class="generic-success">Submitted successfully.</div>
        <div class="generic-error">An error occurred while processing your request. Please try again.</div>
    </div>
</form>
