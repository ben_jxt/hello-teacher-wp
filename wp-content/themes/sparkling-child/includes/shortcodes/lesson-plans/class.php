<?php

class JXT_Shortcode_LessonPlans
{

    protected $tag = 'jxt_lesson_plans';
    protected $action = 'jxt_lesson_plans';

    public function __construct()
    {
        add_shortcode($this->tag, array($this, 'content'));
    }

    public function content($atts, $content = '')
    {
        $user = wp_get_current_user();

        $query = new WP_Query(array(
            'post_type' => 'lesson_plan',
            'author' => $user->ID,
            'post_status' => 'any'
        ));

        $action = isset($_GET['action']) ? $_GET['action'] : null;

        ob_start();

        if ($action == 'delete')
        {
            include 'delete.php';
        }
        else
        {
            $page_lesson_plans = get_page_link(PAGE_LESSON_PLANS);
            $page_edit_lesson_plan = get_page_link(PAGE_EDIT_LESSON_PLAN);           

            include 'list.php';

            wp_reset_postdata();
        }

        $output = ob_get_contents();

        ob_end_clean();

        return $output;
    }

}

new JXT_Shortcode_LessonPlans();
