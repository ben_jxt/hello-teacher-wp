<?php
if(!empty($_GET['success']))
{
	echo '<div class="alert alert-success">You lesson plan was added successfully.</div>';
}
?>
<?php if ($query->have_posts()) : ?>
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <td>Lesson Plan</td>
                <td>&nbsp;</td>
            </tr>
        </thead>
        <tbody>
            <?php while ($query->have_posts()) : ?>
                <?php
                $query->the_post();
                $post = $query->post;
                $meta_data = get_post_meta($post->ID);
                ?>
                <tr>
                    <td><?php echo esc_html(get_the_title()); ?></td>
                    <td>
                        <a href="<?php echo $page_lesson_plans . '?action=delete&id=' . get_the_ID(); ?>" title="Delete" onclick="return jxt.deleteLessonPlan();"><span class="glyphicon glyphicon-remove"></span></a>
                        <a href="<?php echo $page_edit_lesson_plan . '?id=' . get_the_ID(); ?>" title="Edit"><span class="glyphicon glyphicon-edit"></span></a>
                    </td>
                </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    <?php sparkling_paging_nav(); ?>
<?php else: ?>
    <p><?php echo 'No lesson plans found.'; ?></p>
<?php endif; ?>
