<?php

add_action("wp_ajax_jxt_upload_thumbnail", 'jxt_upload_file');
add_action("wp_ajax_jxt_upload_file1", 'jxt_upload_file');
add_action("wp_ajax_jxt_upload_file2", 'jxt_upload_file');

function jxt_upload_file()
{
    $theme_dir = get_stylesheet_directory();

    include $theme_dir . '/js/jquery-file-upload/server/php/UploadHandler2.php';
    
    $upload_dir = wp_upload_dir();
    
    $sub_dir = $upload_dir['subdir'];

    $dir = $upload_dir['path'] . '/';
    
    @mkdir($dir, 0755, true);   

    $options = array();
    $options['upload_dir'] = $dir;
    $options['download_via_php'] = false;
    $options['access_control_allow_methods'] = array('POST');
    $options['accept_file_types'] = '/\.(gif|jpe?g|png|txt|doc|docx|pdf|rtf|xls|xlsx|zip|rar)$/i';
    $options['image_versions'] = array(
        '' => array(
            'max_width' => 1024,
            'max_height' => 1024,
            'jpeg_quality' => 100
        )
    );

    $handler = new \UploadHandler2($options, false);

    $result = $handler->post(false);

    if (!empty($result['files']))
    {
        foreach ($result['files'] as $idx => $file)
        {
            unset($file->delete_url);
            unset($file->delete_type);
            unset($file->url);
            unset($file->size);
            unset($file->type);
            
            $file->path = $sub_dir . '/' . $file->name;

            $result['files'][$idx] = $file;
        }
    }

    echo json_encode($result);

    die();
}
